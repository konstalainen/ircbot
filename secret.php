<?php

/**
 * INSTRUCTIONS HOW TO USE THIS FILE
 * 
 * Modify this PHP-file and move it in a location in the internet where people can post messages
 * Set up the bot again and replace the URL with the location of this PHP-file
 * Change the rights of the file where the messages are stored.
 * Restart the bot
 * 
 * Bot reads messages from the URL where the GET-parameter with name "read" has the value of 1
 * Bot deletes a message from the URL where the GET-parameter with name "clear" has a value of the timestamp of the desired message
 * 
 * SENDING A MESSAGE
 * 
 * Go to the URL where you've uploaded this file.
 * Input the message and the receiver.
 * The "Confirm key" is the 10 first characters from the md5-hash of the SALT and the name of the receiver
 * 	- You can feel free to modify this. It doesn't change the bot functionality.
 * 
 **/

// This is the location of the file where the messages are stored.
define("MSG_PATH", "s3cretm3ssag3fil3.dat");

// This is the salt that is added before the receiver name.
define("SALT", "s4ltys4l7");

header("Charset: utf-8");

// Get messages from the file
function getMessages() {
	
	
	if($contents = file_get_contents(MSG_PATH)) { // Check, if file can be read.
		echo $contents; // Echo them, so bot can read and post them.
	}
	
	else echo ""; // Reading the file failed, so print nothing.
	
}

// Delete messages that have the desired timestamp
function deleteMessage($timestamp) { 
	
	// All the messages, that don't have this timestamp, will be preserved and saved back to the file.
	
	if($file_contents = file_get_contents(MSG_PATH)) { // Check if the file can be read.
		
		$contents = ""; // Storage for those messages, that won't be deleted.
		$messages = explode("\n", $file_contents); // Get every row = every message into an array.
		
		foreach($messages as $message) { // Get every message and check the timestamp.
			
			$parts = explode("|", $message); // A message has a structure of PRIVMSG|timestamp|receiver|message
			
			if(count($parts) < 2)
				continue; // Don't let us check the faulty messages that don't have the structure. Remove them anyway.
			
			if($parts[1] != $timestamp)
				$contents .= $message; // Store those messages, that don't match with the timestamp that we want to delete. 
			
		}
		
		file_put_contents(MSG_PATH, $contents); // Save the remaining messages back to the file.
		
	}
	
}

// Post a message.
// *** NOTE: If writing new messages constantly fails, check that you have the rights to write and read the file!
function postMessage($post = array()) {
	
	if(!isset($post["message"], $post["hashcode"], $post["receiver"]))
		return false; // The message must have all the information. Let us not add faulty messages.
	
	
	foreach($post as $key => $value)
		$$key = $value; // Get the variables from the array into variables, that are named by the array keys.
		
	if($hashcode != substr(md5(SALT . strtolower($receiver)) ,0, 10))
		return false; // If the posted Confirmation key does not match with the desired structure of the hash, don't add the message.
	
	// Add the message into the file where the messages are stored.
	// The parts of the message are separated with the character | and the messages are separated with the newline \n
	// Message form is: PRIVMSG|<timestamp>|<receiver>|<message>\n
	$return = file_put_contents(
	
			MSG_PATH, 
			
				"PRIVMSG|" . time() . "|" . 
				str_replace(array("\n", "\r", "|"), array("", "", ""), $receiver) . "|" . 
				str_replace(array("\n", "\r", "|"), array("", "", ""), $message) . "\n",
				// Remove newlines and | separators from the message and the receiver so there won't be too much bugs
				
			FILE_APPEND // Append to the file, if there are older messages in the queue.
			
			); 
	
	if($return)
		return true; // Writing into the file was successful!
	
	else
		return false; // If couldn't write into the file, let us know about it
	
	
}

if(isset($_GET["read"]) && $_GET["read"] == 1)
	getMessages(); // We print the messages if URL is "<URL>?read=1"

elseif(isset($_GET["clear"]) && is_numeric($_GET["clear"]) && $_GET["clear"] > 0)  // Check if timestamp is set and it's numeric and not incorrect
	deleteMessage($_GET["clear"]); // We erase some messages if URL is "<URL>?clear=<timestamp>"

else { // Otherwise, let us print the form.
?>

<html>
	<head>
		<meta charset="utf-8" />
		<title>Send a new message</title>
	</head>
	
	<body style="padding: 2em">

<?php
	
	if(isset($_POST["receiver"], $_POST["hashcode"], $_POST["message"])) { // If the user has posted information and all the fields are good
		// Then we will call the postMessage function to add the new message and get the return value 
		if(postMessage($_POST)) { // It returns true if we could post a new message?>
			
			<span style="color: green;">Viesti lähetetty</span>
<?php
		 } else { // Otherwise it will return false.
			 ?>
			 
			<span style="color: red;">Viestin lähetys epäonnistui</span>
			 
<?php 	}
		
	} ?>
	
	<!-- This is the actual form. You can edit this but remember to have all the required field here. -->
	<form action="secret.php" method="post">
		
		<h2>Send a message</h2>
		<input style="padding: 0.3em; margin: 0.3em;" size="30" type="text" value="" name="receiver" placeholder="Receiver"/><br />
		<input style="padding: 0.3em; margin: 0.3em;" size="30"  type="text" value="" name="hashcode" placeholder="Confirm key" /><br />
		<input style="padding: 0.3em; margin: 0.3em;" size="50" type="text" value="" name="message" placeholder="Message" /><br />
		<input type="submit" value="Send" />
		
	</form>
	

	</body>
</html>

<?php }
