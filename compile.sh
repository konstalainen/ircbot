#!/bin/bash
echo "Recompiling the Java source files..."
javac -d . -encoding utf-8 ircbot/application/*.java
echo "Application classes compiled"
javac -d . -encoding utf-8 ircbot/helpers/*.java
echo "Helper classes compiled"
javac -d . -encoding utf-8 ircbot/setup/*.java
echo "Setup classes compiled"
javac -d . -encoding utf-8 ircbot/statistics/*.java
echo "Stat generator classes compiled"
javac -encoding utf-8 *.java
echo "Main class compiled"
echo "Ready."
