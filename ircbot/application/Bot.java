package ircbot.application;

import java.io.*;
import java.util.*;
import ircbot.helpers.Tiedosto;

public class Bot implements Serializable {
	
	private String login;
	private String salasana;
	private String realname;
	private ArrayList<String> nicknames;
	private String url_address;
	private boolean loaded;
	private Kanava mainchannel;
	private Kayttaja admin;
	private String logfolder;
	private ArrayList<String> servers;
	private TimeZone tz;

	public Bot() {
	
		this.login = Information.PROJECT_NAME;
		this.salasana = null;
		this.realname = Information.PROJECT_NAME + " v" + Information.VERSION;
		String[] temp = {Information.PROJECT_NAME, Information.PROJECT_NAME + "^", Information.PROJECT_NAME + "-", Information.PROJECT_NAME + "_"};
		this.nicknames = new ArrayList<String>(Arrays.asList(temp));
		this.url_address = "";
		this.mainchannel = null;
		this.admin = null;
		this.logfolder = "logs/";
		String[] temp2 = {"ircnet.eversible.com","irc.portlane.se:6666","irc.portlane.se:6665","irc.portlane.se","irc.atw-inter.net","irc.atw-inter.net:6665","irc.atw-inter.net:6666","ircnet.blacklotus.net", "irc.fu-berlin.de:6665","irc.fu-berlin.de:6670","irc.uni-erlangen.de","irc.us.ircnet.net:6665","irc.us.ircnet.net:6666","irc.us.ircnet.net","irc.us.ircnet.net:6668"};
		this.servers = new ArrayList<String>(Arrays.asList(temp2));
		this.tz = TimeZone.getTimeZone("GMT");
		this.loaded = false;
	}
	
	public boolean isLoaded() {
		return this.loaded;
	}
	
	public void setValues(String login, String salasana, String realname, ArrayList<String> nicknames, String url_address, Kanava kanava, Kayttaja kayttaja, String folder, ArrayList<String> servers, String timezone) {
	
		this.loaded = true;
		this.salasana = salasana;
		this.login = login;
		this.realname = realname;
		this.nicknames = nicknames;
		this.url_address = url_address;
		this.mainchannel = kanava;
		this.admin = kayttaja;
		this.logfolder = folder;
		this.servers = servers;
		this.tz = TimeZone.getTimeZone(timezone);
		
		Tiedosto.tallennaBotti(this);
	}
	
	public boolean allValuesSet() {
		if(this.login == null)
			return false;
		if(this.realname == null)
			return false;
		if(this.salasana == null)
			return false;
		if(this.nicknames == null)
			return false;
		if(this.nicknames.size() == 0)
			return false;
		if(this.mainchannel == null)
			return false;
		if(this.admin == null)
			return false;
		if(this.servers == null)
			return false;
		if(this.servers.size() == 0)
			return false;
		if(this.tz == null)
			return false;
		
		return true;
	}
	
	public String getLogin() {
		return this.login;
	}
	public String getSalasana() {
		return this.salasana;
	}
	public String getRealname() {
		return this.realname;
	}
	public ArrayList<String> getNicknames() {
		return this.nicknames;
	}
	public String getUrl() {
		return this.url_address;
	}
	public Kanava getMainChannel() {
		return this.mainchannel;
	}
	public Kayttaja getDefaultAdmin() {
		return this.admin;
	}
	public String getFolder() {
		return this.logfolder;
	}
	
	public ArrayList<String> getServers() {
		return this.servers;
	}

	public TimeZone getTimeZone() {
		return this.tz;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setSalasana(String salasana) {
		this.salasana = salasana;
	}
	
	public void setRealname(String realname) {
		this.realname = realname;
	}
	
	public void setNicknames(ArrayList<String> nicknames) {
		this.nicknames = nicknames;
	}
	
	public void setMainChannel(Kanava channel) {
		this.mainchannel = channel;
	}
	
	public void setDefaultAdmin(Kayttaja admin) {
		this.admin = admin;
	}
	
	public void setUrl(String url) {
		this.url_address = url;
	}
	
	public void setFolder(String folder) {
		this.logfolder = folder;
	}
	
	public void setServers(ArrayList<String> servers) {
		this.servers = servers;
	}

	public void setTimeZone(TimeZone tz) {
		this.tz = tz;
	}
	
	public String toString() {
		String str = "Nicknames:\n";

		if(this.nicknames == null)
			str = str + "\t(empty)";

		else if(this.nicknames.size() == 0)
			str = str + "\t(empty)";

		else {
			for(int i = 0; i < this.nicknames.size(); i++)
				str = str + "\t" + this.nicknames.get(i) + ((i < (this.nicknames.size() - 1)) ? "\n" : "");
		}

		str = str + "\n\nMain user:\n";

		if(this.admin == null)
			str = str + "\t(empty)";
		else 
			str = str + "\tNickname: " + this.admin.getNick() + "\n\tPassword: " + this.admin.getPassword();

		str = str + "\n\nBot master password:\n";

		if(this.salasana == null)
			str = str + "\t(empty)";
		else 
			str = str + "\t" + this.salasana;

		str = str + "\n\nIdent:\n\t" + this.login + "\n\nReal name:\n\t" + this.realname;
		str = str + "\n\nMain channel:\n";

		if(this.mainchannel == null)
			str = str + "\t(empty)";
		else 
			str = str + "\t" + this.mainchannel.nimi() + ((this.mainchannel.salasana().length() > 0) ? ", key:" + this.mainchannel.salasana() : "");

		str = str + "\n\nServers:\n";

		if(this.servers == null)
			str = str + "\t(empty)";

		else if(this.servers.size() == 0)
			str = str + "\t(empty)";

		else {
			for(int i = 0; i < this.servers.size(); i++)
				str = str + "\t" + this.servers.get(i) + ((i < (this.servers.size() - 1)) ? "\n" : "");
		}

		str = str + "\n\nURL-buffer:\n\t" + this.url_address + "\n\nPath to log files\n\t" + this.logfolder;
		
		return str;
	}
	
}
