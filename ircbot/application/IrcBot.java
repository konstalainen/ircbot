package ircbot.application;

import java.io.*;
import java.net.*;
import java.util.*;
import ircbot.helpers.*;
/**
 * This is a Java-based IRC-bot
 * 
 * Features:
 * <ul>
 * 		<li>Auto-ops on join or with !op command to defined users</li>
 * 		<li>Auto-voices with !voice command</li>
 * 		<li>Password-protected administration</li>
 * 		<li>Add and remove admin/op/voice rights on fly</li>
 * 		<li>Join/leave channels</li>
 * 		<li>Log channel messages and tell statistics according to the logs</li>
 * 		<li>Disable logging for specific channels</li>
 * 		<li>Save joined channels and defined users into files for restart</li>
 * 		<li>Send messages via PHP-form</li>
 * </ul>
 * 
 * @version 0.7.2
 * @author Konsta Mäenpänen <maenpanen9@gmail.com>
 * 
 */
public class IrcBot {

// Botin asetukset
public static String nick;
public static ArrayList<String> nickLista = new ArrayList<String>();
public static String login = null;
public static String realName = null;
public static String server = null;
public static String salasana = null;
public static String url_address = null;
public static Kayttaja default_admin = null;
public static Kanava main_channel = null;
public static String log_folder = null;
public static TimeZone timezone = null;
public static long lastMsg = System.currentTimeMillis();



// Vakioasetukset
public static final String NICK = "IrcBot";
public static final String LOGIN = "IrcBot";
public static final String REALNAME = Information.PROJECT_NAME + " v" + Information.VERSION;
public static final String SALASANA = "BOT_PASSWORD";
public static final String URL_ADDRESS = "http://localhost/path_to_directory/buffer.php";
	// Oletuskäyttäjän asetukset
	public static String admin_nick_DEF = "ADMIN_NICK";
	public static String admin_password_DEF = "ADMIN_PASSWORD";
	public static String admin_host_DEF = "default@admin.host";
	public static Kayttaja DEFAULT_ADMIN = new Kayttaja(admin_nick_DEF, admin_password_DEF, admin_host_DEF);
public static Kanava MAIN_CHANNEL = new Kanava("#default_channel", "", true);
public static final String LOG_FOLDER = "logs/";

// Tietueet
public static Tietokanta tietokanta = new Tietokanta();
public static ArrayList<String> serverit;
public static ArrayList<String> tervehdykset = new ArrayList<String>();
public static ArrayList<Kanava> channels = new ArrayList<Kanava>();

// Yhdistysoliot
public static Socket socket;
public static BufferedWriter writer;
public static BufferedReader reader;

public static void run() throws Exception {
	// Botin käynnistys
	System.out.println("\n\n\t" + Information.PROJECT_NAME.toUpperCase() + "\n\t[version " + Information.VERSION + "]\n\n");
	System.out.println("\tKonsta Maenpanen\n\t<maenpanen9@gmail.com>\n\t<antti.maenpanen@student.uwasa.fi>\n");
	System.out.println("\t== BETA ==\n\n\tStarting up...\n");
	
	// Luodaan bin-kansio, jos ei ole luotu vielä.
	System.out.println("Checking for file folders and creating them");
	Tiedosto.luoHakemisto("bin");
	
	
	/* ******************************************
				ASETUSTEN LATAUS
				NIMIMERKKIEN LATAUS
	 ******************************************** */
	 
	 
	System.out.println("Getting bot settings");
	Logger.log("Launching the bot", 4);
	Bot ircbotti = new Bot();
	
	if(!(new File("bin/botinfo.dat").isFile())) {
		System.out.println("!>> You have not set up the bot!");
		System.out.println("!>> Please run setup before launching");
		System.out.println("TYPE \"java Config\" or \"setup\" or \"./setup.sh\" to launch setup");
		return;
	}
	
	ircbotti = Tiedosto.lataaBotti();
	
	if(false) {
		String[] nicks = {"Bot","B0t","Kbot","KoBot","K0Bot","k0b0t","b0tk0t","yb0t"};		 
		String[] servers = {"ircnet.eversible.com", "irc.portlane.se","irc.atw-inter.net","ircnet.blacklotus.net","irc.jyu.fi","irc.ircnet.ee","irc.snt.utwente.nl","irc.sci.kun.nl","irc.xs4all.nl"};
		ircbotti.setValues(LOGIN, SALASANA, REALNAME, new ArrayList<String>(Arrays.asList(nicks)), URL_ADDRESS, MAIN_CHANNEL, DEFAULT_ADMIN, LOG_FOLDER, new ArrayList<String>(Arrays.asList(servers)), "Europe/Helsinki");
	}
	
	login = ircbotti.getLogin();
	salasana = ircbotti.getSalasana();
	realName = ircbotti.getRealname();
	default_admin = ircbotti.getDefaultAdmin();
	url_address = ircbotti.getUrl();
	main_channel = ircbotti.getMainChannel();
	log_folder = ircbotti.getFolder();
	serverit = ircbotti.getServers();
	nickLista = ircbotti.getNicknames();
	timezone = ircbotti.getTimeZone();
	
	if(!ircbotti.allValuesSet()) {
		
		System.out.println("!>> All the bot values may not have been set.");
		System.out.println("!>> Please run the setup program to set the bot up");
		System.out.println("TYPE \"java Config\" or \"setup\" or \"./setup.sh\" to launch setup");
		Logger.log("Some of the bot values were null in the startup", 8);
		return;
	}

	if(log_folder.endsWith("/")) {
		Tiedosto.luoHakemisto(log_folder.substring(0, log_folder.length() - 2));
	}
	
	/* ******************************************
				KÄYTTÄJIEN LATAUS
	 ******************************************** */
	 
	 
	 
	System.out.println("Loading users.");
	
	tietokanta = Tiedosto.lataaKayttajat();
	if(tietokanta.userCount() == 0) {

		// Luodaan admin-käyttäjät
		tietokanta.addUser(default_admin);
		tietokanta.addAdmin(default_admin.getNick());
		
		if(!Tiedosto.tallennaKayttajat(tietokanta)) {
			System.out.println("!>> Failed to save default users to database!");
			}
	}

	
	
	/* ******************************************
				KANAVIEN LATAUS
	 ******************************************** */
	 
	channels = Tiedosto.lataaKanavat();
	
	if(channels.size() == 0) {
		System.out.println("!>> Channel list is empty or file was not found");
		System.out.println("Loading default channel.");
		channels.add(main_channel);
		System.out.println("Saving the default channel.");
		Tiedosto.tallennaKanavat(channels);
	}
	
	// Latausosio loppuu
	System.out.println("!>> BOT INITIALIZED SUCCESFULLY!");
	
	
	/* ******************************************
				VALMISTAUTUU KÄYNNISTYMÄÄN
	 ******************************************** */
	 
	Logger.log("Bot information loaded", 4); 
	System.out.println("There are " + serverit.size() + " servers.");
	System.out.println("There are " + tietokanta.userCount() + " registered users.");
	System.out.println("There are " + channels.size() + " channels.");

	// Nykyinen kanava
	Kanava channel;
    
	// Muuttuja, jolla evätään ettei turhia viestejä voi lähettää useita. (Deprecated)
	int message_count = 0;
	
	/* ******************************************
				YHTEYDEN YLLÄPITO
	 ******************************************** */
	 
	// Muuttuja, jossa tarkistus, monennellako listan servereistä ollaan (uudelleenyhdistys).
	int server_count = 0; 
	 
	main_loop:
	while (true) {
		// Nykyiseksi serveriksi valitaan serverilistan server_count:nnes alkio, aluksi 0.
		server = serverit.get(server_count);
		int server_port = 6667;
		
		// jos portti on asetettu eriksi kuin 6667
		String[] server_split = server.split(":");
		
		if(server_split.length == 2) {
		
			if(server_split[0].length() > 0)
				server = server_split[0];
			
			if(server_split[1].length() > 0) {
				try {
					server_port = Integer.parseInt(server_split[1]);
				} catch(NumberFormatException e) {
					server_port = 6667;
					System.out.println("!>> Error reading the port number");
					Logger.log("Port number " + server_split[1] + " failed the integer conversion", 8);
					Logger.log(e, 8);
				}
				}
			}
		
		// Kasvatetaan serverilaskuria
		server_count++;
		if(server_count == (serverit.size())) // Jos kaikki serverit on käyty läpi, nollataan ja aletaan alusta.
			server_count = 0;
			
		try { // Socketin luonti
			socket = new Socket();
			System.out.println("Creating an empty socket...");
			socket.connect(new InetSocketAddress(server, server_port), 10000); // yritä yhdistää 10 sekunnin ajan, sitten vaihdetaan
			System.out.println("Trying to connect to " + server + ":" + server_port);
			Logger.log("Trying to connect to to " + server + ":" + server_port, 4);
			}
		catch(SocketTimeoutException e) { // Timeout ylitetään
			System.out.println("!>> Connection timed out!");
			Logger.log("Connection to " + server + ":" + server_port + " timed out", 5);
			Logger.log(e, 5);
			continue;
		}
		catch(IOException e) { // Jos socketin luonti epäonnistuu, aloitetaan alusta toisella osoitteella.
			System.out.println("!>> Failed to create socket to " + server + "!");
			Logger.log("Connection to " + server + ":" + server_port + " failed", 5);
			Logger.log(e, 5);
			continue;
		}
		
		socket.setSoTimeout(15*60*1000); // Jos 15 minuutin aikana ei kuulu mitään, edes pingpongia, yhteys on varmaan katki
		System.out.println("Socket created succesfully!");
		Logger.log("Connected to " + server, 4);
		// Luodaan sisään- ja ulostulovirtaukset (streamit) nykyiselle socketille
		
		
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        System.out.println("Stream readers and writers generated");
		
		
		// UUTTA
		//UrlLukija url_lukija = new UrlLukija(writer);
		
		
		// Lähetetään palvelimelle tieto käyttäjästä (nimimerkki, käyttäjänimi ja oikea nimi)
		nick = nickLista.get(0);
        writer.write("NICK " + nick + "\r\n");
        writer.write("USER " + login + " 8 * : " + realName + "\r\n");
        writer.flush();
		
        System.out.println("NICK: " + nick);
		System.out.println("USER: " + login);
		
        // Viestit palvelimelta
        String line = null;
		
		// Onko kirjautumisviesti vastaanotettu (tätä ennen ei tarkasteta url-bufferia)
		boolean connected = false;
		
        for (int i = 1; ((line = reader.readLine()) != null); i++) { // Tässä loopissa tarkastetaan kirjautumistiedot
            if (line.indexOf("004") >= 0) { // Nyt on kirjauduttu
				System.out.println("Logged in to IRC server");
				System.out.println(Parser.toChat(line, nick));
				Logger.log("Successfully connected to " + server + " as " + nick, 4);
                break; // Hypätään ulos kirjautumistietoloopista
            }
            else if (line.indexOf("433") >= 0) { // Nimimerkki käytössä
                System.out.println("!>> Nickname " + nick + " is already in use");
				System.out.println(Parser.toChat(line, nick));
				Logger.log("Nickname " + nick + " was reserved", 7);
				
				if(i < nickLista.size()) { // Pompataan seuraavaan nimimerkkiin
					nick = nickLista.get(i);
					writer.write("NICK " + nick + "\r\n");
					writer.flush();
					System.out.println("Asking for a new nickname " + nick);
					
					}
				else {
					Logger.log("No free nicks were found", 7);
					System.out.println("!>> No free nicks were found");
					continue main_loop; // Jos kaikki nickit käyty läpi, aloitetaan main-loop alusta = vaihdetaan palvelinta.
					}
            }

			else if (line.toLowerCase().startsWith("ping ")) { // Vastataan pingauksiin, jos palvelin lähettää niitä jo tässä vaiheessa
				System.out.println(Parser.toChat(line, nick));
				writer.write("PONG " + line.substring(5) + "\r\n");
				writer.flush();
				System.out.println(Parser.toChat("PONG " + line.substring(5), nick));
			}
			
			System.out.println("Listening to the server...");
			if(line.indexOf("376") >= 0 && !connected) { // Yhdistetty palvelimelle, web-bufferia voidaan lukea
				Logger.log("Connected", 4);
				System.out.println("CONNECTED!");
				connected = true;
				}
        }
        
		for(int i = 0; i < channels.size(); i++) { // Liitytään kanaville
			channel = channels.get(i);
			send("JOIN " + channel.nimi() + ((channel.salasana().length() > 0) ? " " + (channel.salasana()) : "" ));
			Logger.log("Joining " + channel.nimi() + ", password: " + channel.salasana(), 3);
		}
		
		// Lähetetään tieto kanavalle 
		send("PRIVMSG " + MAIN_CHANNEL.nimi() + " :Connected to " + server + ":" + server_port); 
		
		// UUTTA
		// URL-LUKIJAN SÄIE
		UrlLukija lukeminen = new UrlLukija(writer, url_address);
		
		Thread luku_thread = new Thread(lukeminen);
		luku_thread.setDaemon(true);
		luku_thread.start();
		// ------------------
		
		// TILASTOLUOJAN SÄIE
		StatsGenerator html_stats = new StatsGenerator(channels, log_folder, "html/");
		
		Thread html_thread = new Thread(html_stats);
		html_thread.setDaemon(true);
		html_thread.start();
		// ------------------
		
        // Looppi, jossa luetaan serveriltä vastaanotettu viesti line-merkkijonoon, kunnes mitään ei enää saada
        // TODO: parempi viestien "parsaus"
        while ((line = reader.readLine()) != null) {
		
		
			if (line.toLowerCase().startsWith("ping ")) { // Vastataan pingauksiin
				System.out.println(Parser.toChat(line, nick));
				send("PONG " + line.substring(5));
				System.out.println(Parser.toChat("PONG " + line.substring(5), nick));
			}
			
			else if(line.indexOf("376") >= 0 && !connected) { // Jos yhdistäminen ehdittiin skipata kirjautumisloopissa tehdään se vielä täällä.
				System.out.println("CONNECTED!");
				Logger.log("Connected", 4);
				connected = true;
				}
				
		else { // Muut toiminnot

			/*
			Automaattinen +o moodi ennalta-annetuille käyttäjille.
			*/
			if(Parser.autoOp(line, tietokanta)) {
			
				System.out.println("Operator joined.");
				send("MODE " + Parser.getChannel(line) + " +o " + Parser.getNick(line));

				}
			
			/*
			Jos vastaanotetaan yksityisviesti
			*/
			if(Parser.isPrivateMsg(line, nick))
				{
				
				/*
				Admin suorittaa komennon (ADMIN)
				*/
				if(tietokanta.isHostAdmin(Parser.getHost(line)) && !Parser.getMsg(line).startsWith("login") && !Parser.getMsg(line).startsWith("logout") && !Parser.getMsg(line).startsWith("changepw")) {
				
					/*
						ADMIN
						Uuden operaattorin lisäys
					*/
					if(Parser.getMsg(line).startsWith("addop ")) {
					
						String[] osat = Parser.getMsg(line).substring(6).split("#");
						
						if(osat.length == 2) {
						
							osat[1] = "#" + osat[1];
							
							if(tietokanta.addOp(osat[0], osat[1])) {
								Logger.log(Parser.getNick(line) + " +o " + osat[0] + " @ " + osat[1], 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Added op to user " + osat[0] + " on " + osat[1] + ".");
								Tiedosto.tallennaKayttajat(tietokanta);
								}
							
							else {
								Logger.log(Parser.getNick(line) + " failed +o " + osat[0] + " @ " + osat[1], 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Failed to add op. Check user and channel.");
								}
								
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line) , 2);
							send("PRIVMSG " + Parser.getNick(line) + " :Failed to add op. Type \"addop nick#channel\".");
							}
						}
						
					/*
						ADMIN
						Operaattorin poisto
					*/
					else if(Parser.getMsg(line).startsWith("delop ")) {
					
						String[] osat = Parser.getMsg(line).substring(6).split("#");
						
						if(osat.length == 2) {
						
							osat[1] = "#" + osat[1];
							
							if(tietokanta.delOp(osat[0], osat[1])) {
								Logger.log(Parser.getNick(line) + " -o " + osat[0] + " @ " + osat[1], 2);
								Tiedosto.tallennaKayttajat(tietokanta);
								send("PRIVMSG " + Parser.getNick(line) + " :Deleted the op!");
								}
								
							else {
								Logger.log(Parser.getNick(line) + " failed -o " + osat[0] + " @ " + osat[1], 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Failed to delete op. Check user and channel.");
								}
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 2);
							send("PRIVMSG " + Parser.getNick(line) + " :Failed to delete op. Type\"delop nick#channel\".");
							}
						}
					
					/*
						ADMIN
						Auto-voicen lisäys
					*/
					else if(Parser.getMsg(line).startsWith("addvoice ")) {
					
						String[] osat = Parser.getMsg(line).substring(9).split("#");
						
						if(osat.length == 2) {
						
							osat[1] = "#" + osat[1];
							
							if(tietokanta.addVoice(osat[0], osat[1])) {
								Logger.log(Parser.getNick(line) + " +v " + osat[0] + " @ " + osat[1], 2);
								Tiedosto.tallennaKayttajat(tietokanta);
								send("PRIVMSG " + Parser.getNick(line) + " :Added voice to " + osat[0] + " on " + osat[1] + ".");
								}
								
							else {
								Logger.log(Parser.getNick(line) + " failed +v " + osat[0] + " @ " + osat[1], 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Failed to add voice. Check user and channel.");
								}
							}
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 2);
							send("PRIVMSG " + Parser.getNick(line) + " :Failed to add voice. Type \"addvoice nick#channel\".");
							}
						}
					
					/*
						ADMIN
						Auto-voicen poisto
					*/
					else if(Parser.getMsg(line).startsWith("delvoice ")) {
					
						String[] osat = Parser.getMsg(line).substring(9).split("#");
						
						if(osat.length == 2) {
						
							osat[1] = "#" + osat[1];
							
							if(tietokanta.delVoice(osat[0], osat[1])) {
								Logger.log(Parser.getNick(line) + " -v " + osat[0] + " @ " + osat[1], 2);
								Tiedosto.tallennaKayttajat(tietokanta);
								send("PRIVMSG " + Parser.getNick(line) + " :Deleted the voice!");
								}
								
							else {
								Logger.log(Parser.getNick(line) + " failed -v " + osat[0] + " @ " + osat[1], 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Failed to delete voice. Check user and channel.");
								}
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 2);
							send("PRIVMSG " + Parser.getNick(line) + " :Failed to delete voice. Type \"delvoice nick#channel\".");
							}
						}
					
					/*
						ADMIN
						Käyttäjän poistaminen (poistaa admin, op ym statukset)
					*/
					else if(Parser.getMsg(line).startsWith("deluser ")) {
					
						String poisto_nick = Parser.getMsg(line).substring(8);
						
						if(poisto_nick.length() > 0) {
						
							if(poisto_nick.toLowerCase().equals(default_admin.getNick().toLowerCase())) {
								Logger.log(Parser.getNick(line) + " tried to delete main admin", 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Cannot delete main admin!");
							}
						
							else if(tietokanta.delUser(poisto_nick)) {
								Logger.log(Parser.getNick(line) + " deleted " + poisto_nick, 2);
								Tiedosto.tallennaKayttajat(tietokanta);
								send("PRIVMSG " + Parser.getNick(line) + " :User " + poisto_nick + " deleted.");
								}
								
							else {
								Logger.log(Parser.getNick(line) + " failed to delete " + poisto_nick, 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Failed to delete user. Check that the user exists");
								}
							
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 2);
							send("PRIVMSG " + Parser.getNick(line) + " :Username was not given. Type \"deluser nick\".");
							}
						
						}
					/*
						ADMIN
						Kanavalle liittyminen yhdistämisen jälkeen
					*/
					else if(Parser.getMsg(line).startsWith("join ")) {
					
						String[] osat = Parser.getMsg(line).split(" ");
						String liityttava = ""; // joko "#kanava salasana" tai "#kanava"
						String liityttava_npw = ""; // Liityttävä kanava ilman mahdollista salasanaa
						String liityttava_password = ""; // Liityttävän kanavan salasana
						
						if(osat.length > 2) {
						
							liityttava = osat[1] + " " + osat[2];
							liityttava_npw = osat[1];
							liityttava_password = osat[2];
							
							}
							
						else if(osat.length == 2) {
						
							liityttava = osat[1];
							liityttava_npw = osat[1];
							
							}
						
						if(liityttava.length() > 0) {
						
							send("JOIN " + liityttava);
							send("PRIVMSG " + Parser.getNick(line) + " :Tried to join " + liityttava_npw);
							
							channels.add(new Kanava(liityttava_npw, liityttava_password));
							html_stats.editChannels(new Kanava(liityttava_npw, liityttava_password), true);
							Tiedosto.tallennaKanavat(channels);
							Logger.log(Parser.getNick(line) + " added the channel " + liityttava, 3);
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 3);
							send("PRIVMSG " + Parser.getNick(line) + " :Channel name was not given. Type \"join #channel\".");
							}
						
						}
						
					/*
						ADMIN
						Kanavalta poistuminen
					*/
					else if(Parser.getMsg(line).startsWith("leave ")) {
					
						String[] osat = Parser.getMsg(line).split(" ");
						
						if(osat.length == 2) {
						
							int chan_rm_index = -1;
							
							for(int klkm = 0; klkm < channels.size(); klkm++) {
							
								if(channels.get(klkm).nimi().toLowerCase().equals(osat[1].toLowerCase())) {
									chan_rm_index = klkm;
									break;
								}
							
							}
							
							if(chan_rm_index != -1) {
															
								if(channels.size() == 1)
									send("PRIVMSG " + Parser.getNick(line) + " :The bot must be on at least one channel!");
									
								else {
									send("PRIVMSG " + osat[1] + " :Viszontlátásra!");
									send("PART " + osat[1] + " :Leaving!");
									send("PRIVMSG " + Parser.getNick(line) + " :Left the channel " + osat[1] + "");
									
									html_stats.editChannels(channels.get(chan_rm_index), false);
									channels.remove(chan_rm_index);
									Tiedosto.tallennaKanavat(channels);
									Logger.log(Parser.getNick(line) + " removed channel " + osat[1], 3);
								}
							}
							
							else 
								send("PRIVMSG " + Parser.getNick(line) + " :There is not such channel.");
							
						}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 3);
							send("PRIVMSG " + Parser.getNick(line) + " :Channel name was not given. Type \"leave #channel\".");
							}
						
						}
						
					/*
						ADMIN
						Nimimerkin vaihto, vaihtaa myös nick-muuttujan, jos nickin vaihto onnistuu
					*/
					else if(Parser.getMsg(line).startsWith("nick ")) {
					
						String[] osat = Parser.getMsg(line).split(" ");
						
						if(osat.length > 1) {
						
							send("NICK :" + osat[1]);
							String temp_line = reader.readLine();
							String[] temp_osat = temp_line.split(" NICK :");
							
							if(temp_osat.length == 2)	{
							
								if(temp_osat[1].equals(osat[1])) {
								
									nick = osat[1];
									send("PRIVMSG " + Parser.getNick(line) + " :Bot nickname was changed successfully!");
									Logger.log(Parser.getNick(line) + " changed the bot nickname to " + osat[1], 4);
									}
									
								else {
									Logger.log(Parser.getNick(line) + " failed to change bot nickname to " + osat[1], 7);
									send("PRIVMSG " + Parser.getNick(line) + " :Nickname couldn't be changed. This nick may already be in use.");
									}
								
								}
								
							else {
								Logger.log(Parser.getNick(line) + " failed to change bot nickname to " + osat[1], 7);
								send("PRIVMSG " + Parser.getNick(line) + " :Nickname couldn't be changed. Server didn't response to the request.");
								}
							
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 3);
							send("PRIVMSG " + Parser.getNick(line) + " :New nickname was not given. Type \"nick newnickname\".");
							}
						
						}
						
					/*
						ADMIN
						Viestin lähettäminen vastaanottajalle
					*/
					else if(Parser.getMsg(line).startsWith("msg ")) { 
					
						String[] osat = Parser.getMsg(line).split(" ");
						
						if(osat.length > 2) {
						
							if(osat[1].startsWith("#"))
								Tiedosto.tallennaLogi(log_folder, osat[1], nick, Parser.getMsg(line).substring(("msg " + osat[1]).length() + 1), false, timezone);
								
							String message_to_be_sent = osat[1] + " :" + Parser.getMsg(line).substring(("msg " + osat[1]).length() + 1);
							send("PRIVMSG " + message_to_be_sent);
							send("PRIVMSG " + Parser.getNick(line) + " :Sent a message to " + osat[1] + ".");
							Logger.log(Parser.getNick(line) + " sent a message to " + osat[1], 1);
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 1);
							send("PRIVMSG " + Parser.getNick(line) + " :Receiver and message were not given. Type \"msg receiver your message\".");
							}
						
						}
					
					/*
						ADMIN
						Vaaditaan botin uudelleenkäynnistäminen
						Aloittaa main-loopin alusta (luo uuden socketin uudelle palvelimelle)
					*/
					else if(Parser.getMsg(line).equals("reboot " + salasana)) {

						send("PRIVMSG " + Parser.getNick(line) + " :Bot will now try to reconnect. This may take time, if new server can't be found.");
						send("QUIT :Rebooting... I'll be back.");
						Logger.log(Parser.getNick(line) + " rebooted the bot", 7);
							
						}
					
					/*
						ADMIN
						Sulkee koko bottiohjelman. Vaatii uudelleenkäynnistämisen
					*/
					else if(Parser.getMsg(line).equals("quit " + salasana)) {
						
						Logger.log(Parser.getNick(line) + " shut the bot", 8);
						send("PRIVMSG " + Parser.getNick(line) + " :Bot has quitted..");
						send("QUIT :Disconnected by the admin");
						reader.readLine();
						return;
						}
						
					
					/*
						ADMIN
						Listaa kaikki botille luodut käyttäjät
					*/
					else if(Parser.getMsg(line).equals("listusers")) {
					
						Logger.log(Parser.getNick(line) + " listed the users", 1);
						send("PRIVMSG " + Parser.getNick(line) + " :" + tietokanta.userCount() + " users.");
						
						for(int i = 0; i < tietokanta.userCount(); i++) {
						
							Kayttaja temp_kayttaja = tietokanta.getUserByIndex(i);
							
							if(temp_kayttaja == null)
								continue;

							String osat = temp_kayttaja.getNick();
							
							if(temp_kayttaja.isAdmin())
								osat = osat + " (admin)";
								
							else {
							
								if(temp_kayttaja.opCount() > 0)
									osat = osat + " (" + temp_kayttaja.opCount() + " op)";
									
								if(temp_kayttaja.voiceCount() > 0)
									osat = osat + " (" + temp_kayttaja.voiceCount() + " voice)";

								}
								
							send("PRIVMSG " + Parser.getNick(line) + " :" + osat + "");
							Thread.sleep(1000);
							
							}
							
						send("PRIVMSG " + Parser.getNick(line) + " :End of list. Get additional info with \"userinfo nick\".");
						
						}
						
					/*
						ADMIN
						Listaa tietoa käyttäjästä
					*/						
					else if(Parser.getMsg(line).startsWith("userinfo ")) {
					
						Logger.log(Parser.getNick(line) + " listed info about " + Parser.getMsg(line).substring(9), 1);
						
						if (tietokanta.nickIndex(Parser.getMsg(line).substring(9)) == -1)
							send("PRIVMSG " + Parser.getNick(line) + " :User wasn't found");
							
						else {
							
							Kayttaja temp_kayttaja = tietokanta.getUserByIndex(tietokanta.nickIndex(Parser.getMsg(line).substring(9)));
							
							if(temp_kayttaja != null) {
							
								send("PRIVMSG " + Parser.getNick(line) + " :" + temp_kayttaja.opCount() + " op, " + temp_kayttaja.voiceCount() + " voice");
								send("PRIVMSG " + Parser.getNick(line) + " :Op channels: " + temp_kayttaja.opList());
								send("PRIVMSG " + Parser.getNick(line) + " :Voice channels: " + temp_kayttaja.voiceList());
								send("PRIVMSG " + Parser.getNick(line) + " :Logged in on: " + temp_kayttaja.getHost());
								
								}
								
							else {
								Logger.log(Parser.getNick(line) + " failed to search for " + Parser.getMsg(line).substring(9) + "", 1);
								send("PRIVMSG " + Parser.getNick(line) + " :The given user wasn't found!");
								}
							
							}
							
						}
						
					/*
						ADMIN
						Poistaa hostilta adminoikeudet
					*/						
					else if(Parser.getMsg(line).startsWith("deladmin ")) {
						
						if(Parser.getMsg(line).substring(9).toLowerCase().equals(default_admin.getNick().toLowerCase())) {
							send("PRIVMSG " + Parser.getNick(line) + " :Cannot remove admin rights from default admin!");
							Logger.log(Parser.getNick(line) + " tried to delete main admin rights", 2);
						}
						
						else if(tietokanta.delAdmin(Parser.getMsg(line).substring(9))) {
							Tiedosto.tallennaKayttajat(tietokanta);
							send("PRIVMSG " + Parser.getNick(line) + " :Removed admin rights from " + Parser.getMsg(line).substring(9));
							Logger.log(Parser.getNick(line) + " removed admin rights of " + Parser.getMsg(line).substring(9), 2);
							}
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 2);
							send("PRIVMSG " + Parser.getNick(line) + " :Failed to delete admin. List users with \"listusers\".");
							}
						
						}
						
					/*
						ADMIN
						Suorittaa IRC-raakakomennon
					*/
					else if(Parser.getMsg(line).startsWith("rawcom ")) {
						
						String raakakomento = Parser.getMsg(line).substring(7);
						
						if(!raakakomento.startsWith("NICK")) {

							send(raakakomento);
							send("PRIVMSG " + Parser.getNick(line) + " :Executed an IRC raw command...");
							Logger.log(Parser.getNick(line) + " executed raw command " + Parser.getMsg(line), 1);
							}
							
						else {
							Logger.log(Parser.getNick(line) + " tried to use NICK raw command", 1);
							send("PRIVMSG " + Parser.getNick(line) + " :Insufficient command! NICK command cannot be used because it may crash the bot.");
							}
						
						}
						
					/*
						ADMIN
						Poistaa viestien tallennuksen palvelimelle kanavalta
					*/						
					else if(Parser.getMsg(line).startsWith("dontlog ")) {
					
						int chan_index = -1;
					
						for(int clkm = 0; clkm < channels.size(); clkm++) {
							
							if(channels.get(clkm).nimi().toLowerCase().equals(Parser.getMsg(line).substring(8)))
								chan_index = clkm;
								
						}
						
						if(chan_index != -1) {
							
							channels.get(chan_index).asetaLogi(false);
							Tiedosto.tallennaKanavat(channels);
							send("PRIVMSG " + Parser.getNick(line) + " :Removed logging on the channel.");
							send("PRIVMSG " + Parser.getMsg(line).substring(8) + " :Kanavan logien talletus poistettu.");
							Logger.log(Parser.getNick(line) + " removed logging on " + Parser.getMsg(line).substring(8), 3);
						}
							
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 3);
							send("PRIVMSG " + Parser.getNick(line) + " :Failed to unlog. Type \"dontlog #channel\".");
							}
						}
						
					/*
						ADMIN
						Tallentaa viestit palvelimelle, jos on aiemmin poistettu
					*/						
					else if(Parser.getMsg(line).startsWith("savelog ")) {
					
						int chan_index = -1;
					
						for(int clkm = 0; clkm < channels.size(); clkm++) {
							
							if(channels.get(clkm).nimi().toLowerCase().equals(Parser.getMsg(line).substring(8)))
								chan_index = clkm;
								
						}
						
						if(chan_index != -1) {
							
							channels.get(chan_index).asetaLogi(true);
							Tiedosto.tallennaKanavat(channels);
							send("PRIVMSG " + Parser.getNick(line) + " :Now continuing to log the channel.");
							send("PRIVMSG " + Parser.getMsg(line).substring(8) + " :Kanavan logit tallennetaan.");
							Logger.log(Parser.getNick(line) + " restored logging on " + Parser.getMsg(line).substring(8), 3);
						}
							
							
						else {
							Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 3);
							send("PRIVMSG " + Parser.getNick(line) + " :Log setting failed. Type \"savelog #channel\".");
							}
						}
					
					/*
						ADMIN
						Rekisteröi uuden käyttäjän
					*/
					else if(Parser.getMsg(line).startsWith("register ")) {
						
						String[] osat = Parser.getMsg(line).substring(9).split(" ");
						
						if(osat.length == 2) {
							
							if(tietokanta.addUser(new Kayttaja(osat[0], osat[1]))) {
								
								send("PRIVMSG " + Parser.getNick(line) + " :User " + osat[0] + " registered with password " + osat[1]);
								send("PRIVMSG " + osat[0] + " :" + Parser.getNick(line) + " rekisteröi sinut botille nickillä " + osat[0] + " ja salasanalla " + osat[1]);
								send("PRIVMSG " + osat[0] + " :Sisäänkirjautuminen yksityisviestillä: \"login <nick> <salasana>\" - Sisäänkirjautumisen jälkeen saat lisäohjeita yksityisviestillä \"help\".");
								Logger.log(Parser.getNick(line) + " registered " + osat[0], 2);
								Tiedosto.tallennaKayttajat(tietokanta);
								}
								
							else
								send("PRIVMSG " + Parser.getNick(line) + " :Cound't register because of possible duplicate nickname.");
							}
							
							else {
								Logger.log(Parser.getNick(line) + " misspelled " + Parser.getMsg(line), 2);
								send("PRIVMSG " + Parser.getNick(line) + " :Coundn't register the user, type \"register user password\"");
								}
						}
						
					/*
						ADMIN
						Lähettää ohjeita
					*/
					else if(Parser.getMsg(line).startsWith("help")) {
					
						if(Parser.getMsg(line).equals("help addop")) 
							send("PRIVMSG " + Parser.getNick(line) + " :addop user#channel - Adds op to user. No space before #.");
							
						else if(Parser.getMsg(line).equals("help delop"))
							send("PRIVMSG " + Parser.getNick(line) + " :delop user#channel - Removes op from the user.");
							
						else if(Parser.getMsg(line).equals("help listusers")) 
							send("PRIVMSG " + Parser.getNick(line) + " :Lists all the users. If there are plenty, this may take some time.");
							
						else if(Parser.getMsg(line).equals("help deladmin")) 
							send("PRIVMSG " + Parser.getNick(line) + " :Removes admin rights of the user.");
							
						else if(Parser.getMsg(line).equals("help join")) 
							send("PRIVMSG " + Parser.getNick(line) + " :join #channel [password] - Joins a channel and the channel is stored into database.");
							
						else if(Parser.getMsg(line).equals("help leave")) 
							send("PRIVMSG " + Parser.getNick(line) + " :leave #channel - Removes channel from the database. Leaves the channel if bot is on it.");

						else if(Parser.getMsg(line).equals("help nick")) 
							send("PRIVMSG " + Parser.getNick(line) + " :nick new_nickname - Changes the bot nickname.");
							
						else if(Parser.getMsg(line).equals("help msg")) 
							send("PRIVMSG " + Parser.getNick(line) + " :msg receiver message - Sends a message to receiver.");
							
						else if(Parser.getMsg(line).equals("help quit")) 
							send("PRIVMSG " + Parser.getNick(line) + " :quit password - Shuts down the bot. Requires password");
							
						else if(Parser.getMsg(line).equals("help reboot")) 
							send("PRIVMSG " + Parser.getNick(line) + " :reboot password - Disconnects and tries to reconnect. Requires password.");
							
						else if(Parser.getMsg(line).equals("help userinfo")) 
							send("PRIVMSG " + Parser.getNick(line) + " :userinfo nick - Information about the user.");
							
						else if(Parser.getMsg(line).equals("help rawcom")) 
							send("PRIVMSG " + Parser.getNick(line) + " :rawcom COMMAND - Sends unparsed IRC command.");
							
						else if(Parser.getMsg(line).equals("help dontlog")) 
							send("PRIVMSG " + Parser.getNick(line) + " :dontlog #channel - Stops loggin the channel.");
							
						else if(Parser.getMsg(line).equals("help savelog")) 
							send("PRIVMSG " + Parser.getNick(line) + " :savelog #channel - Continues logging the channel if logging is stopped.");
							
						else {
							Logger.log(Parser.getNick(line) + " asked for help ", 1);
							send("PRIVMSG " + Parser.getNick(line) + " :Commands: addop, delop, listusers, userinfo, deladmins, deluser, join, leave, nick, msg, rawcom, reboot, quit, dontlog, savelog, saveusers, savechans, register - Type help command for more info. Commands should be written in lowercase.");
							}
						}
					
					/*
						ADMIN
						Virheellinen komento
					*/
					else {
						send("PRIVMSG " + Parser.getNick(line) + " :Command not found. Type help for help. Commands are in lowercase.");
						}
					}
				
				else if(Parser.getMsg(line).startsWith("login ")) {
					
					String[] osat = Parser.getMsg(line).substring(6).split(" ");;
					
					if(osat.length == 2) {
						
						if(tietokanta.login(osat[0], osat[1], Parser.getHost(line))) {
							Logger.log(Parser.getNick(line) + " has logged in (" + osat[0] + "::" + Parser.getHost(line) + ")", 1);
							send("PRIVMSG " + Parser.getNick(line) + " :Olet kirjautunut sisään osoitteelta " + Parser.getHost(line) + ". Muista kirjautua ulos komennolla \"logout <nick> <salasana>\", jos osoite on yleisessä käytössä.");
							Tiedosto.tallennaKayttajat(tietokanta);
						}
						
						else
							Logger.log(Parser.getNick(line) + " tried to log in (" + osat[0] + "::" + Parser.getHost(line) + ")", 1);
					}
					
					else
						Logger.log(Parser.getNick(line) + " misspelled the login command " + Parser.getMsg(line), 1);
				}
				
				else if(Parser.getMsg(line).startsWith("logout ")) {
				
					String[] osat = Parser.getMsg(line).substring(7).split(" ");
					
					if(osat.length == 2) {
						
						if(tietokanta.logout(osat[0], osat[1])) {
							Logger.log(Parser.getNick(line) + " logged out (" + osat[0] + "::" + Parser.getHost(line) + ")", 1);
							send("PRIVMSG " + Parser.getNick(line) + " :Olet nyt kirjautunut ulos. Voit kirjautua sisään komennolla \"login <nick> <salasana>\".");
							Tiedosto.tallennaKayttajat(tietokanta);
						}
						
						else
							Logger.log(Parser.getNick(line) + " tried to log out (" + osat[0] + "::" + Parser.getHost(line) + ")", 1);
					}
					
					else
						Logger.log(Parser.getNick(line) + " misspelled the logout command " + Parser.getMsg(line), 1);
						
				}
				
				else if(Parser.getMsg(line).startsWith("changepw ")) {
					
					String[] osat = Parser.getMsg(line).substring(9).split(" ");
					
					if(osat.length == 3) {
						
						if(tietokanta.changepass(osat[0], osat[1], osat[2])) {
							Logger.log(Parser.getNick(line) + " changed the password", 2);
							send("PRIVMSG " + Parser.getNick(line) + " :Password changed");
							Tiedosto.tallennaKayttajat(tietokanta);
						}
						
						else
							Logger.log(Parser.getNick(line) + " tried to change password (" + osat[0] + "::" + Parser.getHost(line) + ")", 2);
					}
					
					else
						Logger.log(Parser.getNick(line) + " tried to change password (" + osat[0] + "::" + Parser.getHost(line) + ")", 2);
					
				}
				
				else if(Parser.getMsg(line).startsWith("help")) {
				
					if(tietokanta.hostIndex(Parser.getHost(line)) > -1) {
					
						send("PRIVMSG " + Parser.getNick(line) + " :Kirjaudu sisään: \"login <nick> <salasana>\", ulos: \"logout <nick> <salasana>\", vaihda salasana: \"changepw <nick> <vanha_salasana> <uusi_salasana>\"");
						send("PRIVMSG " + Parser.getNick(line) + " :Kanavalla operaattorina: !op !dop !voice !devoice !topic !kick !ban !kickban !dontlog !savelog !mute !unmute");
						Logger.log(Parser.getNick(line) + " asked for help", 1);
						
					}
					
					else
						Logger.log("Unregistered user " + Parser.getNick(line) + " tried to ask for help", 2);
					
				}
				/*
					ADMIN
					Uusi admin autentikoi salasanalla
									
				if(Parser.getMsg(line).equals("login " + salasana)) {
				
					if(addAdmin(getHost(line)))
						send("PRIVMSG " + Parser.getNick(line) + " :Sinut lisättiin adminiksi onnistuneesti!");
						
					else
						send("PRIVMSG " + Parser.getNick(line) + " :Lisäys epäonnistui!");
					
					}
				*/
				}
				
			// Komennot, toimivat operaattoreilla tietyillä kanavilla ja admineilla kaikkialla
				
			
			else if(Parser.isChannelMsg(line) && (tietokanta.isHostOp(Parser.getHost(line), Parser.getChannel(line)) || tietokanta.isHostAdmin(Parser.getHost(line))) && Parser.isCommand(Parser.getMsg(line))) {
			
				/*
					KOMENTO
					Antaa käyttäjälle op:t
				*/
				
				if(Parser.getMsg(line).equals("!op")) 
					send("MODE " + Parser.getChannel(line) + " +o " + Parser.getNick(line));
					
				
				/*
					KOMENTO
					Kickaa ja bannaa käyttäjän (bankick)
				*/
				else if(Parser.getMsg(line).startsWith("!kickban ")) {
				
					if(Parser.getHostByNick(Parser.getMsg(line).substring(9), writer, reader, server).length() > 0) {
						send("MODE " + Parser.getChannel(line) + " +b *!*@" + Parser.getHostByNick(Parser.getMsg(line).substring(9), writer, reader, server));
						send("KICK " + Parser.getChannel(line) + " " + Parser.getMsg(line).substring(9) + " :Kick-banned.");
						}

					}
				
				/*
					KOMENTO
					Kickaa käyttäjän
				 */
				else if(Parser.getMsg(line).startsWith("!kick ")) {
				
					if(Parser.getMsg(line).substring(6).length() > 0)  
						send("KICK " + Parser.getChannel(line) + " " + Parser.getMsg(line).substring(6) + " :You're not wanted here");
						
						
					}
				
				/*
					KOMENTO
					Mykistää kanavan
				*/			
				else if(Parser.getMsg(line).equals("!mute")) {
					send("MODE " + Parser.getChannel(line) + " +m");
					}
				
				
				/*
					KOMENTO
					Poistaa mykistyksen
				*/
				else if(Parser.getMsg(line).equals("!unmute")) {
					send("MODE " + Parser.getChannel(line) + " -m");
					}
					
				
				/*
					KOMENTO
					Bannaa annetun käyttäjän koko hostin
				*/
				else if(Parser.getMsg(line).startsWith("!ban ")) {
				
					String host_to_ban = Parser.getHostByNick(Parser.getMsg(line).substring(5), writer, reader, server);
					
					if(host_to_ban.length() > 0) 
						send("MODE " + Parser.getChannel(line) + " +b *!*@" + host_to_ban);
				}	
			
				/*
					KOMENTO
					Antaa operaattorioikeudet max. kuudelle (6) käyttäjälle
				*/			
				else if(Parser.getMsg(line).startsWith("!op ")) {
				
					String[] osat = Parser.getMsg(line).substring(4).split(" ");
					
					if(osat.length > 0 && osat.length < 7) { // Max 6
					
						String op_komento = "+";
						String op_nickit = "";
						
							for(int i = 0; i < osat.length; i++) {
							
								op_komento = op_komento + "o";
								op_nickit = op_nickit + osat[i] + " ";
								
								if(i == 2) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit);
									op_komento = "+";
									op_nickit = "";
									continue;
									}
									
								else if(i == 5 || (i == (osat.length - 1))) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit);
									break;
									}
									
								}
						}
					}
				
				/*
					KOMENTO
					Poistaa operaattorit kanavalta
				*/
				else if(Parser.getMsg(line).startsWith("!dop ")) {
				
					String[] osat = Parser.getMsg(line).substring(5).split(" ");
					
					if(osat.length > 0 && osat.length < 7) { // Max 6
					
						String op_komento = "-";
						String op_nickit = "";
						
							for(int i = 0; i < osat.length; i++) {
								op_komento = op_komento + "o";
								op_nickit = op_nickit + osat[i] + " ";
								
								if(i == 2) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit);
									op_komento = "-";
									op_nickit = "";
									continue;
									}
									
								else if(i == 5 || (i == (osat.length - 1))) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit);
									break;
									}
								}
						}
						
					}
				
				/*
					KOMENTO
					Antaa voicet käyttäjälle
				*/
				else if(Parser.getMsg(line).startsWith("!voice ")) {
				
					String[] osat = Parser.getMsg(line).substring(7).split(" ");
					
					if(osat.length > 0 && osat.length < 7) { // Max 6
					
						String op_komento = "+";
						String op_nickit = "";
						
							for(int i = 0; i < osat.length; i++) {
							
								op_komento = op_komento + "v";
								op_nickit = op_nickit + osat[i] + " ";
								
								if(i == 2) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit);
									op_komento = "+";
									op_nickit = "";
									continue;
									}
									
								else if(i == 5 || (i == (osat.length - 1))) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit);
									break;
									}
								}
						}
					}
				
				/*
					KOMENTO
					Poistaa voicet
				*/
				else if(Parser.getMsg(line).startsWith("!devoice ")) {
				
					String[] osat = Parser.getMsg(line).substring(9).split(" ");
					
					if(osat.length > 0 && osat.length < 7) { // Max 6
						
						String op_komento = "-";
						String op_nickit = "";
						
							for(int i = 0; i < osat.length; i++) {
							
								op_komento = op_komento + "v";
								op_nickit = op_nickit + osat[i] + " ";
								
								if(i == 2) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit + "\r\n");
									op_komento = "-";
									op_nickit = "";
									continue;
									}
									
								else if(i == 5 || (i == (osat.length - 1))) {
									send("MODE " + Parser.getChannel(line) + " " + op_komento + " " + op_nickit);
									break;
									}
								}
						}
						
					}
					
					/*
						KOMENTO
						Poistaa logituksen
					*/
					else if(Parser.getMsg(line).equals("!dontlog")) {
					
						int chan_index = -1;
					
						for(int clkm = 0; clkm < channels.size(); clkm++) {
							
							if(channels.get(clkm).nimi().toLowerCase().equals(Parser.getChannel(line).toLowerCase()))
								chan_index = clkm;
								
						}
						
						if(chan_index != -1) {
							
							channels.get(chan_index).asetaLogi(false);
							send("PRIVMSG " + Parser.getChannel(line) + " :Kanavan logeja ei tallenneta.");
							Tiedosto.tallennaKanavat(channels);
						}
							
							
						else 
							send("PRIVMSG " + Parser.getChannel(line) + " :Logituksen asetus epäonnistui, kanavaa ei ole tallennettu.");
					}
						
					/*
						KOMENTO
						Palauttaa logituksen
					*/
					else if(Parser.getMsg(line).equals("!savelog")) {
					
						int chan_index = -1;
					
						for(int clkm = 0; clkm < channels.size(); clkm++) {
							
							if(channels.get(clkm).nimi().toLowerCase().equals(Parser.getChannel(line).toLowerCase()))
								chan_index = clkm;
								
						}
						
						if(chan_index != -1) {
							
							channels.get(chan_index).asetaLogi(true);
							send("PRIVMSG " + Parser.getChannel(line) + " :Kanavan logit tallennetaan.");
							Tiedosto.tallennaKanavat(channels);
						}
							
							
						else 
							send("PRIVMSG " + Parser.getChannel(line) + " :Logituksen asetus epäonnistui, kanavaa ei ole tallennettu.");
					}
					
					/*
						KOMENTO
						Asettaa kanavan otsikon
					*/
					else if(Parser.getMsg(line).startsWith("!topic ")) 
						send("TOPIC " + Parser.getChannel(line) + " :" + Parser.getMsg(line).substring(7));
					
			}
			
			/*
				KOMENTO
				Antaa auto-voicet autovoicattujen listassa oleville
			*/			
			else if(Parser.getMsg(line).equals("!voice") && tietokanta.isHostVoice(Parser.getHost(line), Parser.getChannel(line)))
				send("MODE " + Parser.getChannel(line) + " +v " + Parser.getNick(line));
				
		
			// Lukee tavaraa palvelimelta joko suoraan tai erilaisten komentojen jälkeen
				
			/*
				LUKU
				Hakee parhaat käyttäjät
			*/	
			else if(Parser.getMsg(line).equals("!stats") && Parser.isChannelMsg(line)) {
				try {
					Stats stats = new Stats(Parser.getChannel(line), log_folder);
					String temp = stats.getTop();
					send("PRIVMSG " + Parser.getChannel(line) + " :" + "Eniten rivejä kanavalla " + Parser.getChannel(line) + ": " + temp);
					temp = stats.getTopWords();
					send("PRIVMSG " + Parser.getChannel(line) + " :" + "Eniten sanoja kanavalla " + Parser.getChannel(line) + ": " + temp);
					temp = stats.getTopSlaps();
					send("PRIVMSG " + Parser.getChannel(line) + " :" + "Väkivaltaisimmat " + Parser.getChannel(line) + " käyttäjät: " + temp);
					} 
					catch(Exception e) {
						Logger.log("Failed to get stats", 8);
						Logger.log(e, 8);
						System.out.println("!>> ERROR " + e);
					}	
				}
				
			/*
				LUKU
				Hakee käyttäjän satunnaisen viestin tällä kanavalla
			*/		
			else if(Parser.getMsg(line).startsWith("!random ") && Parser.isChannelMsg(line))	{
				try {
					String random_msg = (new Stats(Parser.getChannel(line), log_folder)).getRandom(Parser.getMsg(line).substring(8));
					send("PRIVMSG " + Parser.getChannel(line) + " :" + random_msg);
					} 
					catch(Exception e) {
						Logger.log("Failed to get random message", 8);
						Logger.log(e, 8);
						System.out.println("!>> ERROR " + e);}			
				}
				
			/*
				LUKU
				Hakee käyttäjän satunnaisen viestin tällä kanavalla
			*/		
			else if(Parser.getMsg(line).equals("!random") && Parser.isChannelMsg(line))	{
				try {
					String random_msg = (new Stats(Parser.getChannel(line), log_folder)).getRandom();
					send("PRIVMSG " + Parser.getChannel(line) + " :" + random_msg);
					} 
					catch(Exception e) {
						Logger.log("Failed to get random message", 8);
						Logger.log(e, 8);
						System.out.println("!>> ERROR " + e);}			
				}
				
			/*
				LUKU
				Hakee online-listan ProLeaguen kanavalta
			*/				
			else if(Parser.getMsg(line).contains("!online") && Parser.getChannel(line).toLowerCase().equals("#leaguep")) {
				try {
					URL url = new URL("http://proleague.arkku.net/onlinebot.php");
					InputStream url_stream = url.openConnection().getInputStream();
					BufferedReader url_reader = new BufferedReader(new InputStreamReader(url_stream));
					String url_line = null;
					while((url_line = url_reader.readLine()) != null) {
						send("PRIVMSG " + Parser.getChannel(line) + " :" + url_line);
						}
					Logger.log("URL has been read", 1);
					url_reader.close();
					} 
					catch(MalformedURLException e) {
						Logger.log("Malformed URL in the reader", 5);
						Logger.log(e, 5);
						System.out.println("!>> ERROR " + e);}
					catch(IOException e) {
						System.out.println("!>> ERROR: " + e);
						Logger.log("Failed to read the URL", 5);
						Logger.log(e, 5);
						}	
				}
				
			/*
				KOMENTO
				Vastaa joitan hauskaa...
			*/
			else if((Parser.getMsg(line).toLowerCase().contains(nick.toLowerCase()) || (Parser.getMsg(line).contains("!op"))) && Parser.isChannelMsg(line)) {
			
					if(message_count == 0) {
						int random_modulo = (int) (System.currentTimeMillis() % 10);
					
						if(random_modulo < 1)
							send("PRIVMSG " + Parser.getChannel(line) + " :Hehee hauska juttu ... \":D\"");
						else if(random_modulo < 2)
							send("PRIVMSG " + Parser.getChannel(line) + " :Juu just joo");
						else if(random_modulo < 3)
							send("PRIVMSG " + Parser.getChannel(line) + " :Oos hiljaa " + Parser.getNick(line) + " jo");
						else if(random_modulo < 4)
							send("PRIVMSG " + Parser.getChannel(line) + " :oota 5 min " + Parser.getNick(line) + ", kahvipaussi just...");
						else if(random_modulo < 5)
							send("PRIVMSG " + Parser.getChannel(line) + " :Ehdotan perma-kickbania käyttäjälle " + Parser.getNick(line) + ", ilmeisesti sopii muillekin?");
						else if(random_modulo < 6)
							send("PRIVMSG " + Parser.getChannel(line) + " :kamaan, en mä oo mikään sirkusapina, tee temppus ite");
						else if(random_modulo < 7)
							send("PRIVMSG " + Parser.getChannel(line) + " :Oikeesti... seuraavasta jo /kick " +  Parser.getNick(line));
						else if(random_modulo < 8)
							send("PRIVMSG " + Parser.getChannel(line) + " :niinpä niin\r\n");
						else if(random_modulo < 9)
							send("PRIVMSG " + Parser.getChannel(line) + " :" +  Parser.getNick(line) + ", sun nukkumaamenoaika meni jo...");
						else send("PRIVMSG " + Parser.getChannel(line) + " :äääöööö uuuuuuuuuuuu :----------D");
						
						message_count = 1;
						}
					else message_count = 0;
					}
			/*
				Jos mitään muuta ei tapahdu, tallennetaan viesti logeihin (kanavaviestit, ei kieltolistalla)
			*/
			if(Parser.isChannelAction(line)) {
				int channel_index = -1;
				for(int clkm = 0; clkm < channels.size(); clkm++) {
				
					if(channels.get(clkm).nimi().toLowerCase().equals(Parser.getChannel(line).toLowerCase()))
						channel_index = clkm;
						
					}
				if(channel_index != -1) {
				
					if(channels.get(channel_index).logita()) {
						Tiedosto.tallennaLogi(log_folder, Parser.getChannel(line), Parser.getNick(line), Parser.getAction(line), true, timezone);
						}
				}
			}

			else if(Parser.isChannelMsg(line) && !Parser.isAction(Parser.getMsg(line))) {
				int channel_index = -1;
				for(int clkm = 0; clkm < channels.size(); clkm++) {
				
					if(channels.get(clkm).nimi().toLowerCase().equals(Parser.getChannel(line).toLowerCase()))
						channel_index = clkm;
						
					}
				if(channel_index != -1) {
				
					if(channels.get(channel_index).logita()) {
						Tiedosto.tallennaLogi(log_folder, Parser.getChannel(line), Parser.getNick(line), Parser.getMsg(line), false, timezone);
						}
				}
			}

			
				
		// Printataan tekstiä.
		System.out.println(Parser.toChat(line, nick));
		//System.out.println(line);
		}	
		}
		
	
	
	// Viestejä ei enää vastaanoteta palvelimelta, suljetaan socket ja aloitetaan uusi kierros uudella palvelimella
	System.out.println("!>> Connection to " + server + " has been terminated");
	Logger.log("Disconnected from " + server, 5);
	socket.close();
	}
}

public static void send(String mejo) {
	try {

		if((System.currentTimeMillis() - lastMsg < 750) && (mejo.startsWith("PRIVMSG"))) {
			//System.out.println("Sending messages too densely, must wait.");
			Thread.sleep(750);
			lastMsg = System.currentTimeMillis();
		}

		writer.write(mejo + "\r\n");
		writer.flush();
		} catch (IOException e) {
			System.out.println("!>> IO error while sending an irc command!");
			Logger.log("IO error with sending", 5);
			Logger.log(e, 5);
			// e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("!>> Sleeping interrupted when sending!");
			Logger.log("InterruptedException caught", 5);
			Logger.log(e, 5);
			// e.printStackTrace();
		} catch (Exception e) {
			System.out.println("!>> Error while sending an irc command!");
			Logger.log("Error with sending", 5);
			Logger.log(e, 5);
			// e.printStackTrace();
		} 
}

public static String tervehdi(String kanava, String kayttaja) {

	int maara = tervehdykset.size();
	String alku = "PRIVMSG " + kanava + " :";
	String loppu = "\r\n";
	if(maara == 0)
		return alku + "Moi, " + kayttaja + "!" + loppu;
	// Math random: [0,1[
	int indeksi = 0;
	indeksi = (int) Math.floor(maara * Math.random());
	String tervehdys = tervehdykset.get(indeksi);
	tervehdys = tervehdys.replaceFirst("KAYTTAJA",kayttaja);
	tervehdys = alku + tervehdys + loppu;
	return tervehdys;
}



}
