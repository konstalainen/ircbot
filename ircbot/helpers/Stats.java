package ircbot.helpers;
import java.io.*;
import java.text.*;
import java.util.*;

public class Stats {

	public class Message {
	
		public String timestamp;
		public String sender;
		public String msg;
		
		public Message(String timestamp, String sender, String msg) {
			this.timestamp = timestamp; 
			this.sender = sender; 
			this.msg = msg;
		}
	}
	
	private ArrayList<Message> rows;
	private ArrayList<Message> acts;
	
	public Stats(String channel, String logpath) {
	
		rows = new ArrayList<Message>();
		acts = new ArrayList<Message>();
	
		if(channel.startsWith("#"))
			channel = channel.substring(1);
			
		try {
		
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(logpath + channel + ".log")));
			String temp;
			
			while((temp = reader.readLine()) != null) {
			
				String[] temp_parts = temp.split("\t");
				
				if(temp_parts.length < 3) // ei ota tyhjiä rivejä
					continue;
					
				if(temp_parts[1].equals("*")) { // toimintoviesteissä * timestampin jälkeen
				
					if(temp_parts.length < 4)
						continue;
						
					acts.add(new Message(temp_parts[0], temp_parts[2].substring(1, (temp_parts[2].length()-1)), temp_parts[3]));
				}
				
				else
					rows.add(new Message(temp_parts[0], temp_parts[1].substring(1, (temp_parts[1].length()-1)), temp_parts[2]));
				
			}
				
		} catch(NullPointerException e) {
			Logger.log("Failed to generate stats", 4);
			Logger.log(e, 4);
			e.printStackTrace();
		} catch(FileNotFoundException e) {
			Logger.log("Stats not found", 4);
			Logger.log(e, 4);
			//e.printStackTrace();
		} catch(IOException e) {
			Logger.log("Failed to generate stats", 4);
			Logger.log(e, 4);
			//e.printStackTrace();
		} catch(Exception e) {
			Logger.log("Failed to generate stats", 4);
			Logger.log(e, 4);
			//e.printStackTrace();
		}
	}
	
	public String getRandom() {
		
		int random = (int) Math.floor((Math.random() * this.rows.size()));
		
		if(random >= this.rows.size())
			return "Hiljainen kanava, ei viestin viestiä";
			
		return "" + this.rows.get(random).sender + ": \"" + this.rows.get(random).msg + "\"";
	}
	
	public String getRandom(String user) {
		
		ArrayList<String> usermsgs = new ArrayList<String>();
		
		for(int i = 0; i < this.rows.size(); i++) {
		
			if(this.rows.get(i).sender.toLowerCase().equals(user.toLowerCase()))
				usermsgs.add(this.rows.get(i).msg);
			
		}
		
		int random = (int) Math.floor((Math.random() * usermsgs.size()));
		
		if(random >= usermsgs.size())
			return "Käyttäjää " + user + " ei ole tällä kanavalla näkynyt, saati kuulunut";
			
		return "" + user + ": \"" + usermsgs.get(random) + "\"";
	}
	
	public String getTop() throws Exception {
	
		Map senders = new HashMap();
		ArrayList<String> users = new ArrayList<String>();
		
		for(int i = 0; i < this.rows.size(); i++) {
		
			String key = this.rows.get(i).sender;
			int value = 1;
			
			if(senders.containsKey(key)) {
				value = ((int) senders.get(key)) + 1;
				senders.put(key, value);
			}
			
			else {
				senders.put(key, value);
				users.add(key);
				}
		}
		
		String[] top5_users = {"","","","",""};
		int[] top5_count = {0,0,0,0,0};
		
		for(int j = 0; j < 5; j++) { // Käydään kaikki sijat läpi, ensin sija nro 1, sitten 2, jne
		
			for(int i = 0; i < users.size(); i++) { // katsotaan, kenellä on eniten viestejä tällä sijalla.
			
				String user = users.get(i);
				int msg_count = (int) (senders.get(user)); // haetaan viestien lukumäärä hashmapista
				
				if(msg_count > top5_count[j]) { // jos enenmmän viestejä, niin lisätään tälle sijalle uusi nick
					top5_users[j] = user;
					top5_count[j] = msg_count;
				}
			}
			
			users.remove(top5_users[j]); // Poistetaan käyttäjä listauksesta, jottei tule uudestaan alemmille sijoille
		}
		
		// Muodostetaan merkkijono
		String str = "";
		for(int i = 0; i < 5; i++) {
			str = str + ((top5_users[i].length() > 0) ? ("" + top5_users[i] + " (" + top5_count[i] + ") ") : "");
		}
		
		return str;
		
		
	}

	public String getTopWords() throws Exception {

		Map senders = new HashMap();
		ArrayList<String> users = new ArrayList<String>();

		for(int i = 0; i < this.rows.size(); i++) {
		
			String key = this.rows.get(i).sender;
			int value = this.rows.get(i).msg.split(" ").length;
			
			if(senders.containsKey(key)) {
				value += ((int) senders.get(key));
				senders.put(key, value);
			}
			
			else {
				senders.put(key, value);
				users.add(key);
				}
		}

		String[] top5_users = {"","","","",""};
		int[] top5_count = {0,0,0,0,0};
		
		for(int j = 0; j < 5; j++) { // Käydään kaikki sijat läpi, ensin sija nro 1, sitten 2, jne
		
			for(int i = 0; i < users.size(); i++) { // katsotaan, kenellä on eniten viestejä tällä sijalla.
			
				String user = users.get(i);
				int msg_count = (int) (senders.get(user)); // haetaan viestien lukumäärä hashmapista
				
				if(msg_count > top5_count[j]) { // jos enenmmän viestejä, niin lisätään tälle sijalle uusi nick
					top5_users[j] = user;
					top5_count[j] = msg_count;
				}
			}
			
			users.remove(top5_users[j]); // Poistetaan käyttäjä listauksesta, jottei tule uudestaan alemmille sijoille
		}
		
		// Muodostetaan merkkijono
		String str = "";
		for(int i = 0; i < 5; i++) {
			str = str + ((top5_users[i].length() > 0) ? ("" + top5_users[i] + " (" + top5_count[i] + ") ") : "");
		}
		
		return str;

	}
	
	public String getTopSlaps() throws Exception {
	
		Map senders = new HashMap();
		ArrayList<String> users = new ArrayList<String>();

		for(int i = 0; i < this.acts.size(); i++) {
			
			if(!this.acts.get(i).msg.startsWith("action: slap"))
				continue;
				
			String key = this.acts.get(i).sender;
			int value = 1;
			
			if(senders.containsKey(key)) {
				value += ((int) senders.get(key));
				senders.put(key, value);
			}
			
			else {
				senders.put(key, value);
				users.add(key);
				}
		}

		String[] top5_users = {"","","","",""};
		int[] top5_count = {0,0,0,0,0};
		
		for(int j = 0; j < 5; j++) { // Käydään kaikki sijat läpi, ensin sija nro 1, sitten 2, jne
		
			for(int i = 0; i < users.size(); i++) { // katsotaan, kenellä on eniten viestejä tällä sijalla.
			
				String user = users.get(i);
				int msg_count = (int) (senders.get(user)); // haetaan viestien lukumäärä hashmapista
				
				if(msg_count > top5_count[j]) { // jos enenmmän viestejä, niin lisätään tälle sijalle uusi nick
					top5_users[j] = user;
					top5_count[j] = msg_count;
				}
			}
			
			users.remove(top5_users[j]); // Poistetaan käyttäjä listauksesta, jottei tule uudestaan alemmille sijoille
		}
		
		// Muodostetaan merkkijono
		String str = "";
		for(int i = 0; i < 5; i++) {
			str = str + ((top5_users[i].length() > 0) ? ("" + top5_users[i] + " (" + top5_count[i] + ") ") : "");
		}
		
		return str;

	}
}
