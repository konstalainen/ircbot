package ircbot.statistics;
public class Actions {
	
	public static int MIN_VALUE = 0;
	public static int PRIVMSG = 0;
	public static int MODE = 1;
	public static int KICK = 2;
	public static int JOIN = 3;
	public static int PART = 4;
	public static int NOTICE = 5;
	public static int TOPIC = 6;
	public static int MAX_VALUE = 6;

	public static boolean inScope(int value) {

		if(value < MIN_VALUE || value > MAX_VALUE)
			return false;
		else
			return true;
	}
}
