package ircbot.application;

public class Information {
	// includes information and default setting values
	public static final String PROJECT_NAME = "IRCbot by km";
	public static final String VERSION = "0.7.2";
	public static final String AUTHOR = "Konsta Maenpanen";
	public static final String EMAIL = "maenpanen9@gmail.com";
}
