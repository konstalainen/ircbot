package ircbot.application;

import java.util.*;
import java.io.Serializable;
/**
 * Käyttäjä-luokassa on metodit käyttäjän tietojen asettamiseen ja lukemiseen.
 * @author Konsta Mäenpänen
 * @version 1.0
 */
public class Kayttaja implements Serializable {

	private static final long serialVersionUID = 2; // Versio 1;
	private static int id = 0;
	private int tunnus;
	private String hostmask; // NYKYINEN hostmaski, eli millä kirjautunut sisään
	private String nick; // nimimerkki, ei välttämättä sama kuin IRC-nimimerkki
	private String password; // salasana, salaus myöhemmin?!
	private boolean admin;
	private ArrayList<String> op_kanavat;
	private ArrayList<String> voice_kanavat;

	public Kayttaja(String nick, String password, String hostmaski) { // rekisteröinti
		this.admin = false;
		this.nick = nick.toLowerCase();
		this.password = password;
		this.hostmask = hostmaski.toLowerCase();
		this.op_kanavat = new ArrayList<String>();
		this.voice_kanavat = new ArrayList<String>();
		this.tunnus = id;
		id++;
	}

	public Kayttaja(String nick, String password) {
		this(nick, password, ""); // Rekisteröityminen ilman hostmaskia
	}

	// Sisään- ja uloskirjautumiseen vaadittavat salasanat

	public boolean passCheck(String salasana) {
		return (this.password.equals(salasana));
	}

	public boolean login(String salasana, String hostmask) {

		if(passCheck(salasana))
			this.hostmask = hostmask;
			
		else
			return false;
			
		return true;
	}

	public boolean logout(String salasana) {
		
		if(passCheck(salasana))
			hostmask = "";
		
		else
			return false;
			
		return true;
	}
	
	public boolean changepass(String oldpass, String newpass) {
		
		if(passCheck(oldpass) && newpass.length() > 0) {
			this.password = newpass;
			return true;
		}
		
		else
			return false;
	}

/* 
 * Käyttäjän oikeuksien hallinta:
 *
 * lisaaAdmin() = muuttaa käyttäjän botin adminiksi
 * poistaAdmin() = poistaa adminstatuksen
 * onAdmin() = onko käyttäjä admin
 * lisaaOp(kanava) = Lisää operaattoriksi kanavalle
 * poistaOp(kanava) = Poistaa operaattorinta kanavalta
 * lisaaVoice(kanava) = Lisää voice-oikeudet kanavalle
 * poistaVoice(kanava) = Poistaa voice:t kanavalta
 * poistaKaikki[Opt/Voicet]() = Tyhjentää kyseisen taulukon
 * [op/voice]Lkm() = Monellako kanavalla käyttäjä on Op/voice
 * onOp(kanava) = Onko käyttäjä op tällä kanavalla
 * onVoice(kanava) = Onko voice-oikeudet tälle kanavalle
 * [op/voice]Luettelo() = Palauttaa merkkijonon, johon on listattu kanavat
 * kerroId() = palauttaa käyttäjä-id:n
 * haeHost() = palauttaa hostmaskin
 * haeNick() = palauttaa nimimerkin
 */
/**
 * Makes this user a bot administrator
 **/
	public void addAdmin() {
		this.admin = true;
	}

/**
 * Removes administrator rights of this user
 **/
	public void delAdmin() {
		this.admin = false;
	}

/**
 * Checks if user has administrator rights
 *
 * @return true if is admin
 **/
	public boolean isAdmin() {
		return this.admin;
	}

/**
 * Makes this user a channel operator
 *
 * @return true if not already channel operator and channel is given correctly
 *
 **/
	public boolean addOp(String kanava) {
		if(!kanava.startsWith("#"))
			return false;
			
		kanava = kanava.toLowerCase();
		
		if(this.op_kanavat.contains(kanava))
			return false;
			
		this.op_kanavat.add(kanava);
			return true;
	}

/**
 * Removes the operator rights of this user
 *
 * @return true if channel could be removed from the listattu
 *
 **/
	public boolean delOp(String kanava) {
		if(!kanava.startsWith("#"))
			return false;
			
		kanava = kanava.toLowerCase();
		
		if(!this.op_kanavat.contains(kanava))
			return false;
			
		this.op_kanavat.remove(kanava);
		
		return true;
	}

/**
 * Adds voice rights to the user
 *
 * @return true if not already voice rights and channel was given correctly
 *
 **/
	public boolean addVoice(String kanava) {

		if(!kanava.startsWith("#"))
			return false;
			
		kanava = kanava.toLowerCase();
		
		if(this.voice_kanavat.contains(kanava))
			return false;
			
		this.voice_kanavat.add(kanava);
		
		return true;
	}

/**
 * Removes voice rights of the user
 *
 * @return true if channel was found from the list
 *
 **/
	public boolean delVoice(String kanava) {

		if(!kanava.startsWith("#"))
			return false;
			
		kanava = kanava.toLowerCase();
		
		if(!this.voice_kanavat.contains(kanava))
			return false;
			
		this.voice_kanavat.remove(kanava);
		
		return true;
	}

/**
 * Makes the user operator on no channels
 **/
	public void delAllOp() {
		this.op_kanavat.clear();
	}
	
/**
 * Makes the user voiceless on all channels
 **/
	public void delAllVoice() {
		this.voice_kanavat.clear();
	}

/**
 * Get the current host of the user
 *
 * @return the hostmask user has currently logged in
 *
 **/
	public String getHost() {
		return this.hostmask;
	}
	
/**
 * Get the nickname the user
 *
 * @return the nickname
 *
 **/
	public String getNick() {
		return this.nick;
	}
	
/**
 * Get the number of channels where this user has op
 *
 * @return integer value, size of the op-channels list
 *
 **/
	public int opCount() {
		return this.op_kanavat.size();
	}
	
/**
 * Get the number of channels where this user has voice
 *
 * @return integer value, size of the voice-channels list
 *
 **/
	public int voiceCount() {
		return this.voice_kanavat.size();
	}

/**
 * Checks if the user has operator rights
 *
 * @return true if is operator
 *
 **/
	public boolean isOp(String kanava) {

		kanava = kanava.toLowerCase();
		return this.op_kanavat.contains(kanava);
		
	}
	
/**
 * Checks if the user has voice rights
 *
 * @return true if is voiced
 *
 **/
	public boolean isVoice(String kanava) {

		kanava = kanava.toLowerCase();
		return this.voice_kanavat.contains(kanava);
		
	}
	
/**
 * Get the list of channels where the user has operator rights
 *
 * @return a string where channels are separated with a blank space
 *
 **/
	public String opList() {

		String palautettava = "";
		
		for(int i = 0; i < this.op_kanavat.size(); i++)
			palautettava = palautettava + " " + op_kanavat.get(i);
			
		return palautettava;

	}
/**

 * Get the list of channels where the user is voiced
 *
 * @return a string where channels are separated with a blank space
 *
 **/
	public String voiceList() {

		String palautettava = "";
		
		for(int i = 0; i < this.voice_kanavat.size(); i++)
			palautettava = palautettava + " " + voice_kanavat.get(i);
			
		return palautettava;

	}
	
/**
 * Get the static id of the user
 *
 * @return integer value
 *
 **/
	public int getId() {
		return this.tunnus;
	}
	
	public String getPassword() {
		return this.password;
	}

} 
