import ircbot.application.IrcBot;
import ircbot.helpers.Logger;

public class Start {
	public static void main(String[] args) {
		try {
			IrcBot.run();
		} catch(Exception e) {
		Logger.log("Fatal error!", 8);
		Logger.log(e, 8);
		}
	}
}
