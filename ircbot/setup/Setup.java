package ircbot.setup;

import ircbot.helpers.Tiedosto;
import ircbot.helpers.Logger;
import ircbot.application.Bot;
import ircbot.application.Kayttaja;
import ircbot.application.Kanava;

import java.io.*;
import java.util.*;

public class Setup {

	private static Bot ircbotti;
	
	public static void run() {
		Tiedosto.luoHakemisto("bin");
		ircbotti = new Bot();
		ircbotti = Tiedosto.lataaBotti();
		
		String str = "";
		
		System.out.println("\n\t[ === IRCBOT SETUP PROGRAM === ]\n\t\t::: version 0.1 :::\n\n");
		System.out.println("!!! Type \"help\" or \"?\" for commands list");
		System.out.println("!!! Save settings with \"save\" before you exit!");
		System.out.println("!!! Please read the manual, too (README.txt)\n");
		
		while(true) {
			System.out.print("Ircbot >> ");
			str = readStr();
			
			if(str.equals("help") || str.equals("?"))
				printInfo();
				
			else if(str.equals("info")) {
				System.out.println("\t\t[ I R C B O T ]\n\t\n\t::: Genuine IRC bot with Java :::\n\n\tVersion 0.5.0\n\t\n\tSince 2014\n\t<antti.maenpanen@student.uwasa.fi>\n\thttp://uva.fi/~w101297\n\n");
			}
			else if(str.equals("settings")) {
			
				System.out.println(">> Current bot settings\n");
				try {
					System.out.println(ircbotti);
					if(!ircbotti.allValuesSet())
						System.out.println("!!! All the values are not yet set, cannot run the bot!");
					
				} catch(NullPointerException e) {
					System.out.println("NullPointerException caught!\nAll the bot settings may have not been set correctly.");
				}
				System.out.println("End of info");
				}
				
			else if(str.startsWith("realname")) {
				
				if(str.indexOf(' ') == 8 && str.length() > 9) {
					ircbotti.setRealname(str.substring(9));
					System.out.println("Realname set to " + str.substring(9));
					}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.startsWith("login")) {
				
				if(str.indexOf(' ') == 5 && str.length() > 6) {
					ircbotti.setLogin(str.substring(6));
					System.out.println("Login name set to " + str.substring(6));
					}
				
				else
					System.out.println("Insufficient parameters");
			}

			else if(str.startsWith("password")) {
				
				if(str.indexOf(' ') == 8 && str.length() > 9) {
					ircbotti.setSalasana(str.substring(9));
					System.out.println("Bot password set to " + str.substring(9));
					}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.startsWith("url")) {
				
				if(str.indexOf(' ') == 3 && str.length() > 4) {
					ircbotti.setUrl(str.substring(4));
					System.out.println("Url address set to \"" + str.substring(4) + "\"");
					}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.startsWith("logpath")) {
				
				if(str.indexOf(' ') == 7 && str.length() > 8) {
					ircbotti.setFolder(str.substring(8));
					System.out.println("Log path set to \"" + str.substring(8) + "\"");
					}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.startsWith("mainchannel")) {
				
				String[] sstr = str.split(" "); // mainchannel #kanava salasana
				
				if(sstr.length == 2) {
					
					if(sstr[1].length() > 0) {
						ircbotti.setMainChannel(new Kanava(sstr[1]));
						System.out.println("Main channel set to: " + sstr[1]);
						}
					else 
						System.out.println("Channel name cannot be empty");
					
					}
					
				else if(sstr.length == 3) {
					
					if(sstr[1].length() > 0 && sstr[2].length() > 0) {
						ircbotti.setMainChannel(new Kanava(sstr[1], sstr[2]));
						System.out.println("Main channel set to: " + sstr[1] + " (" + sstr[2] + ")");
					}
					else
						System.out.println("Insufficient parameters");
				}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.startsWith("mainuser")) {
				
				String[] sstr = str.split(" "); // mainuser nick pw hostmask
				
				if(sstr.length == 3) {
					
					if(sstr[1].length() > 0 && sstr[2].length() > 0) {
						ircbotti.setDefaultAdmin(new Kayttaja(sstr[1], sstr[2]));
						System.out.println("Main user set to: " + sstr[1] + " pass: " + sstr[2]);
						}
					else 
						System.out.println("Insufficient parameters");
					
					}
					
				else if(sstr.length == 4) {
					
					if(sstr[1].length() > 0 && sstr[2].length() > 0 && sstr[3].length() > 0) {
						ircbotti.setDefaultAdmin(new Kayttaja(sstr[1], sstr[2], sstr[3]));
						System.out.println("Main user set to: " + sstr[1] + " pass: " + sstr[2] + " host: " + sstr[3]);
						}
					else 
						System.out.println("Insufficient parameters");
					
					}
				
				else
					System.out.println("Insufficient parameters");
			}

			else if(str.startsWith("timezone")) {


				if(str.indexOf(' ') == 8 && str.length() > 9) {
					ircbotti.setTimeZone(TimeZone.getTimeZone(str.substring(9)));
					System.out.println("Timezone is now " + TimeZone.getTimeZone(str.substring(9)).getDisplayName());
					}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.startsWith("setnicks")) {
				
				String[] sstr = str.split(" "); // setnicks nick1 nick2 nick3
				ArrayList<String> temp_nicks = new ArrayList<String>();
				
				if(sstr.length <= 1) {
					System.out.println("Insufficient parameters");
				}
				
				else {
					for(int i = 1; i < sstr.length; i++) {
						if(sstr[i].length() > 0)
							temp_nicks.add(sstr[i]);
					}
					
					if(temp_nicks.size() > 0) {
						System.out.println(temp_nicks.size() + " nicknames were set successfully");
						ircbotti.setNicknames(temp_nicks);
					}
					
					else
						System.out.println("No nicknames were added or deleted");
				}
			}
			
			else if(str.startsWith("addnick")) {
			
				if(str.indexOf(' ') == 7 && str.length() > 8) {
					ArrayList<String> temp_nicks = ircbotti.getNicknames();
					
					if(temp_nicks == null)
						temp_nicks = new ArrayList<String>();
					
					temp_nicks.add(str.substring(8));
					ircbotti.setNicknames(temp_nicks);
					System.out.println("Nickname " + str.substring(8) + " added, there are now " + temp_nicks.size() + " nicks");
				}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.startsWith("setservers")) {
				
				String[] sstr = str.split(" "); // setservers serv1 serve2
				ArrayList<String> temp_servers = new ArrayList<String>();
				
				if(sstr.length <= 1) {
					System.out.println("Insufficient parameters");
				}
				
				else {
					for(int i = 1; i < sstr.length; i++) {
						if(sstr[i].length() > 0)
							temp_servers.add(sstr[i]);
					}
					
					if(temp_servers.size() > 0) {
						System.out.println(temp_servers.size() + " servers were set successfully");
						ircbotti.setServers(temp_servers);
					}
					
					else
						System.out.println("No servers were added or deleted");
				}
			}
			
			else if(str.startsWith("addserver")) {
			
				if(str.indexOf(' ') == 9 && str.length() > 10) {
					ArrayList<String> temp_servers = ircbotti.getServers();
					
					if(temp_servers == null)
						temp_servers = new ArrayList<String>();
					
					temp_servers.add(str.substring(10));
					ircbotti.setServers(temp_servers);
					System.out.println("Server " + str.substring(10) + " added, there are now " + temp_servers.size() + " servers");
				}
				
				else
					System.out.println("Insufficient parameters");
			}
			
			else if(str.equals("erase") || str.startsWith("erase ")) {
				
				if(str.equals("erase")) {
					ircbotti = new Bot();
					System.out.println("All the values were erased and set to default.");
					System.out.println("You can still undo this by exiting the program without saving.");
				}
				
				else if(str.equals("erase realname")) {
					ircbotti.setRealname(null);
					System.out.println("Real name erased");
				}
				// [realname/login/url/logpath/channel/admin/nicks/servers]
				else if(str.equals("erase login")) {
					ircbotti.setLogin(null);
					System.out.println("Login name erased");
				}
				
				else if(str.equals("erase url")) {
					ircbotti.setUrl("");
					System.out.println("URL set to default value");
				}
				
				else if(str.equals("erase logpath")) {
					ircbotti.setFolder("");
					System.out.println("Log path erased");
				}
				
				else if(str.equals("erase channel")) {
					ircbotti.setMainChannel(null);
					System.out.println("Main channel erased");
				}

				else if(str.equals("erase timezone")) {
					ircbotti.setTimeZone(null);
					System.out.println("Timezone erased");
				}
				
				else if(str.equals("erase admin")) {
					ircbotti.setDefaultAdmin(null);
					System.out.println("Main admin erased");
				}
				
				else if(str.equals("erase nicks")) {
					ircbotti.setNicknames(null);
					System.out.println("Nicknames erased");
				}
				
				else if(str.equals("erase servers")) {
					ircbotti.setServers(null);
					System.out.println("Servers erased");
				}
				
				else
					System.out.println("Insufficient parameters");
			}
				
			else if(str.equals("save")) {
				try {
					Tiedosto.tallennaBotti(ircbotti);
					System.out.println("The bot was saved successfully");
					} catch(NullPointerException e) {
						System.out.println("NullPointerException caught!\nAll the bot settings may have not been set correctly.");
					}
				}
				
			else if(str.equals("exit")) {
				System.out.println("Are you sure you want to quit without saving (Y/N)?");
				str = readStr();
				if(str.equals("Y") || str.equals("y")) {
					System.out.println("Terminating the setup.");
					return;
				}
			}
			
			else {
				System.out.println("Invalid command. Please type \"help\" to see the list of commands.");
			}
		}
		
	}
	
	private static void printInfo() {
		
		System.out.println(
		"=== help / ? ===\nPrint this help.\n" +
		//"help [command]\n\tPrints specified info about the command" + 
		"=== info ===\nPrints information about the bot.\n" +
		"=== settings ===\nPrints the current settings of the bot.\n" +
		"=== realname [value] ===\nSets real name of the bot.\n" + 
		"=== login [value] ===\nSets login info/ident of the bot. In hostmask this would be: nick!login@host\n" +
		"=== url [value] ===\nSets the url address where bot reads data. See manual for specified info.\n" + 
		"=== logpath [value] ===\nSet absolute or relative path where bot should save logs, include trailing slash (/).\n" + 
		"=== timezone [value] ===\nSet timezone where log timestamps should be output. If value is insufficient, GMT will be applied.\n" + 
		"=== mainchannel [channel] (password) ===\nSet the channel where bot joins initially and sends data. Include starting character (#). Password is optional.\n" + 
		"=== mainuser [nick] [password] (hostmask) ===\nSet main administrator. Hostmask (mask@ho.st) is optional. With this login info, the bot can be controlled.\n" +
		"=== password [value] ===\nSet master password to bot. This password is required only when bot is rebooted, or shut via private message.\n" +
		"=== setnicks [nick1] (nick2) (nick3) ... ===\nSet main nickname (nick1) and alternative nicknames for the bot. All current nicks will be erased.\n" + 
		"=== addnick [value] ===\nAdds one extra alternative nick to bot. The older nicks will be preserved.\n" +
		"== setservers [server1(:port)] (server2(:port)) (server3(:port)) ... ===\nSet servers where bot tries to connect. All previous entries will be erased. Port is alternative\n" +
		"=== addserver [server(:port)] ===\nAdd one extra server to the server list of the bot. Older entries will be preserved. Port is 6667 if not set.\n" + 
		"=== erase [realname/login/url/logpath/channel/admin/nicks/servers] ===\nSets the value of given parameter to null or empty without confirmation. User discretion is adviced.\n" + 
		"=== save ===\nSaves the changes you've done in this session.\n" +
		"=== exit ===\nTerminates the setup WITHOUT saving\n"
		);
	}
	
	private static String readStr() {
		Scanner scan = new Scanner(System.in);
		String str = "";
		try {
			str = scan.nextLine();
		} catch(Exception e) {
			str = "";
			System.out.println("Exception was thrown while reading user input.");
			Logger.log("IO-virhe asetuksia laittaessa", 8);
			Logger.log(e, 8);
		}
		
		return str;
	}
	
	private static int readInt() {
	
		int i = Integer.MAX_VALUE;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = "";
		boolean ok = false;
		
		while(!ok) {
		
			str = readStr();
			try {
				i = Integer.parseInt(str);
			} catch(NumberFormatException e) {
				System.out.println("Invalid integer value.");
				ok = false;
				continue;
			}
			
			ok = true;
		}
		
		return i;
	}
}
