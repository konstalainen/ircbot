package ircbot.statistics;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class Message {
	
	protected String contents;
	protected String nick;
	protected Date timestamp;
	protected boolean isAction;
	protected int actionType;


	protected static SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public Message() {

		this.timestamp = new Date();
		this.nick = "";
		this.contents = "";
		this.isAction = false;
		this.actionType = Actions.PRIVMSG;

	}

	public Message(Date timestamp, String nick, String contents, boolean isAction, int actionType) {

		this.timestamp = timestamp;
		this.nick = nick;
		this.contents = contents;
		this.isAction = isAction;
		this.actionType = actionType;
	}


	public Message(String timestamp, String nick, String contents, boolean isAction, int actionType) throws ParseException {
		
		this(dateformat.parse(timestamp), nick, contents, isAction, actionType);
		
	}

	public Message(String rowfromlog) throws ParseException {

		this();

		String[] line = rowfromlog.split("\t");

		if(line.length < 3)
			return;

		else if(line[1].equals("*") && line.length != 4)
			return;

		String logtime = line[0].replaceFirst("\\[","").replaceFirst("\\]","");

		this.timestamp = dateformat.parse(logtime);
		this.isAction = line[1].equals("*");

		if(this.isAction) {
			this.contents = line[3];
			this.nick = line[2].replaceFirst("<","").replaceFirst(">","");
		}

		else {
			this.contents = line[2];
			this.nick = line[1].replaceFirst("<","").replaceFirst(">","");
			this.actionType = Actions.PRIVMSG;
			return;
		}

		if(this.contents.startsWith("action: ")) {
			this.actionType = Actions.PRIVMSG;
			this.contents = this.contents.substring(8);
			
			if(this.contents.length() == 0)
				this.contents = " ";
		}

		else if(this.contents.startsWith("set mode: ")) {
			this.actionType = Actions.MODE;
		}

		else if(this.contents.startsWith("kicked ")) {
			this.actionType = Actions.KICK;
		}

		else if(this.contents.startsWith("has joined")) {
			this.actionType = Actions.JOIN;
		}

		else if(this.contents.startsWith("has left")) {
			this.actionType = Actions.PART;
		}

		else if(this.contents.startsWith("sent notice: ")) {
			this.actionType = Actions.NOTICE;
		}

		else if(this.contents.startsWith("set topic:")) {
			this.actionType = Actions.TOPIC;
		}

		else {
			this.actionType = Actions.PRIVMSG;
			this.isAction = false;
		}

	}

	public boolean malformedMessage() {

		if(this.nick.length() == 0)
			return true;

		else if(this.contents.length() == 0)
			return true;

		else if(!Actions.inScope(this.actionType))
			return true;

		else
			return false;

	}

	private String timeToString() {

		return dateformat.format(this.timestamp);
	}

	public String toString() {
		return this.timeToString() + "\t" + this.nick + ((this.nick.length() < 8) ? "\t" : "") + "\t" + (this.isAction ? ("*" + this.actionType + "* ") : "") + this.contents;
	}



}
