package ircbot.helpers;

import java.io.*;
import java.net.*;
import java.util.*;
import java.text.SimpleDateFormat;
import ircbot.application.Tietokanta;

public class Parser {

	public static boolean autoOp(String mejo, Tietokanta kayttajat) {
		
		String host = getHost(mejo);
		String channel = getChannel(mejo);
		String[] osat = mejo.split(" ");

		if(host.equals("") || channel.equals("") || osat.length < 3)
			return false;

		if(osat[1].equals("JOIN")) {

			if(kayttajat.isHostOp(host, channel))
				return true;
			if(kayttajat.isHostAdmin(host))
				return true;
		}

		return false;
	}

	public static String getHostByNick(String nickname, BufferedWriter writer, BufferedReader reader, String server) throws IOException {
	
		String host = "";
		writer.write("WHOIS " + nickname + "\r\n");
		writer.flush();
		System.out.println("Whois: " + nickname);
		String line;
		for(int i = 0; i < 5; i++) {
			line = reader.readLine();
			if(line == null)
				break;
			if(line.toLowerCase().startsWith(":" + server.toLowerCase() + " 311")) {
				String[] osat = line.toLowerCase().split(" ");
				if(osat.length < 5)
					break;
				else {
					host =  osat[5].toLowerCase();
					System.out.println("Host: " + host);
					break;
					}
				}
			}
		return host;
	}
	
	public static String toChat(String mejo, String nick) {

		Date currentDate = new Date(); // Päiväys timestampia varten.
		SimpleDateFormat dateform = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // muoto [2016-01-31 23:15:59]
		TimeZone timezone = Tiedosto.getTimeZone();
		dateform.setTimeZone(timezone);
		String ts = dateform.format(currentDate); // muuttaa merkkijonoksi

		ts = "[" + ts + "]\t";

		String[] osat = mejo.split(" ");
		int taulun_koko = osat.length;

		if(mejo.toLowerCase().startsWith("ping ")) {
			return ts + mejo;
		}

		if(taulun_koko < 3)
			return ts + mejo;

		if(isChannelAction(mejo))
			return ts + "* " + getChannel(mejo) + ": " + getNick(mejo) + " " + getAction(mejo);

		if(isPrivateMsg(mejo, nick))
			return ts + "<" + getNick(mejo) + "> " + getMsg(mejo);

		if(isChannelMsg(mejo))
			return ts + "<" + getNick(mejo) + " @ " + getChannel(mejo) + "> " + getMsg(mejo);
		
		
		else if(osat[2].equals(nick) && taulun_koko >= 4) {
		
			if(osat[1].equals("433"))
				return ts + "* nickname " + getNick(mejo) + " is already in use";
				
			else if(osat[1].equals("004"))
				return ts + "* server modes: " + appender(mejo, 3);
				
			else if(osat[1].equals("005"))
				return ts + "* server info: " + appender(mejo, 3);
			
			else if(osat[1].equals("042"))
				return ts + "* your unique id: " + osat[3];
				
			else if(osat[1].equals("375"))
				return ts +  "* =================== ( M O T D ) ===================";
				
			else if(osat[1].equals("372"))
				return ts + "* " + appender(mejo, 4);
				
			else if(osat[1].equals("376"))
				return ts + "* =================== (MOTD ended) ==================";
			
			else if(osat[1].equals("332"))
				return ts + "* Topic is: " + appender(mejo, 4);
			
			else if(osat[1].equals("333"))
				return ts + "* Topic set by " + appender(mejo, 4);
				
			else return ts + "* " + appender(mejo, 3);
		}
		else return ts + "* " + mejo;
		}
		
	private static String appender(String[] parts, int start_index) {
		String str = "";
		
		if(start_index >= parts.length || start_index < 0)
			return str;
			
		for(int i = start_index; i < parts.length; i++) {
		
			if(i == start_index && parts[i].startsWith(":"))
				str = str + " " + parts[i].substring(1);
				
			else str = str + " " + parts[i];
		}
		
		return str;
	}

	private static String appender(String mejo, int start_index) {
		String str = "";

		if(start_index >= mejo.split(" ").length || start_index < 0) // merrkijonojokapitäisi XXXjakaa useaaanosaan jeeee ind=4
			return str;

		str = mejo;

		for(int i = 1; i < start_index; i++) {
			str = str.replaceFirst(" ","");
		}

		int space = str.indexOf(' ') + 1;

		if(space == 0 || !(space < str.length()))
			return "";

		str = str.substring(space);

		if(str.startsWith(":"))
			str = str.substring(1);

		return str;

	}
	
	public static String toChat(String mejo) {
		return toChat(mejo, "");
	}

		

	public static String getChannel(String mejo) { // HAKEE KANAVAN
	
		String kanava = "";
		if(mejo.startsWith(":"))
			mejo = mejo.substring(1);
		else return kanava;

		String[] osat = mejo.split(" ");

		int taulun_koko = osat.length;

		if(taulun_koko < 3)
			return kanava;
			
		kanava = osat[2].toLowerCase();
		if(kanava.startsWith(":"))
			kanava = kanava.substring(1);
		if(kanava.startsWith("#"))
			return kanava;
		else return "";
		}

	public static String getNick(String mejo) { // HAKEE NICKIN
		String nick = "";
		if(mejo.startsWith(":"))
			mejo = mejo.substring(1);
		else return "";

		String[] osat = mejo.split(" ");
		int taulun_koko = osat.length;

		if(taulun_koko < 3)
			return "";

		nick = osat[0];
		if(nick.indexOf('!') != -1) {
			nick = nick.substring(0,(nick.indexOf('!')));
			return nick;
			}

		else return "";
		}

	public static String getMsg(String mejo) { // HAKEE VIESTIN
		String viesti = "";
		if(mejo.startsWith(":"))
			mejo = mejo.substring(1);
		else return "";

		String[] osat = mejo.split(" ");

		int taulun_koko = osat.length;
		// :nick!mask@host.com PRIVMSG #kanava :viesti tähän riviin
		if(taulun_koko < 4)
			return "";

		String komento = osat[1];
		if(!komento.equals("PRIVMSG"))
			return ""; // ei ole tällöin yksityisviesti

		for(int i = 3; i < taulun_koko; i++) {
			if(i == 3) {
				if(osat[3].startsWith(":"))
					osat[3] = osat[3].substring(1);
				}
			viesti = viesti + osat[i];
			if(i != (taulun_koko - 1))
				viesti = viesti + " ";
			}
		viesti = viesti.replaceFirst("","");
		viesti = viesti.replaceFirst("","");
		return viesti;

	}

	public static String getAction(String mejo) { // Hakee kanavatoiminnon
		/* 
		 is gay
		:nick!mask@host.com JOIN :#kanava
		:nick!mask@host.comPART#kanava :poistumisviesti hei
		:nick!mask@host.com NOTICE #kanava :ääliö viesti
		:nick!mask@host.com TOPIC #kanava :uusi topic
		:nick!mask@host.comKICK#kanavanimimerkki :olet ääliö
		:nick!mask@host.comMODE#kanava tag (arvo)
		*/
		if(!isChannelAction(mejo))
			return "";

		String[] osat = mejo.split(" ");
		String action = "";
		if(osat.length < 3)
			return "";

		if(osat[1].equals("JOIN"))
			return "has joined";

		if(osat.length < 4)
			return "";

		if(osat[1].equals("PRIVMSG")) {
			
			action = mejo.replaceFirst(" ","").replaceFirst(" ","");

			if((action.indexOf(' ') + 2) < action.length() && (action.indexOf(' ') != -1)) {

				action = action.substring(action.indexOf(' ') + 2);
				action = action.replaceFirst("ACTION","");
				action = action.replaceFirst("","");
				action = "action:" + action;
				return action;
			}

			else
				return "";
		}

		else if(osat[1].equals("PART")) {

			action = mejo.replaceFirst(" ","").replaceFirst(" ","");

			if((action.indexOf(' ') + 2) < action.length() && (action.indexOf(' ') != -1)) {
				action = action.substring(action.indexOf(' ') + 2);
				action = "has left (" + action + ")";
				return action;
			}

			else
				return "has left";
		}

		else if(osat[1].equals("NOTICE")) {

			action = mejo.replaceFirst(" ","").replaceFirst(" ","");

			if((action.indexOf(' ') + 2) < action.length() && (action.indexOf(' ') != -1)) {
				action = action.substring(action.indexOf(' ') + 2);
				action = "sent notice: " + action;
				return action;
			}

			else
				return "";
		}

		else if(osat[1].equals("TOPIC")) {

			action = mejo.replaceFirst(" ","").replaceFirst(" ","");

			if((action.indexOf(' ') + 2) < action.length() && (action.indexOf(' ') != -1)) {
				action = action.substring(action.indexOf(' ') + 2);
				action = "set topic: " + action;
				return action;
			}

			else
				return "";
		}

		else if(osat[1].equals("KICK")) {

			action = mejo.replaceFirst(" ","").replaceFirst(" ","").replaceFirst(" ","");

			if((action.indexOf(' ') + 2) < action.length() && (action.indexOf(' ') != -1)) {
				action = action.substring(action.indexOf(' ') + 2);
				action = "kicked " + osat[4] + " (" + action + ")";
				return action;
			}

			else
				return "kicked " + osat[4];
		}

		else if(osat[1].equals("MODE")) {

			action = mejo.replaceFirst(" ","").replaceFirst(" ","");

			if((action.indexOf(' ') + 1) < action.length() && (action.indexOf(' ') != -1)) {

				action = action.substring(action.indexOf(' ') + 1);
				action = "set mode: " + action;
				return action;
			}
				else
					return "";

		}

		else
			return "";



	}

	public static String getHost(String mejo) { // HAKEE HOSTMASKIN
		if(mejo.startsWith(":"))
			mejo = mejo.substring(1);
		else return "";
		String[] osat = mejo.split(" ");
		int taulun_koko = osat.length;

		if(taulun_koko < 3)
			return "";

		String hosti = osat[0];
		if(hosti.indexOf('!') != -1 && hosti.indexOf('@') != -1) {
			hosti = hosti.substring((hosti.indexOf('!') + 1), (hosti.length()));
			return hosti;
			//System.out.println("Hosti: " + hosti); // DEBUG
		}
		else return "";
		}

		public static boolean isPrivateMsg(String mejo, String nick) { // HAKEE ONKO YKSITYISVIESTI
		
			if(mejo.startsWith(":"))
				mejo = mejo.substring(1);
			else return false;
			// :nick!mask@host.com PRIVMSG #kanava :viesti tähän riviin
			String[] osat = mejo.split(" ");
			int taulun_koko = osat.length;

			if(taulun_koko < 4)
				return false;
			String komento = osat[1];

			if(!komento.equals("PRIVMSG")) // ei ole yksityisviesti
				return false;

			String kayttaja = osat[2].toLowerCase();

			if(kayttaja.startsWith(":"))
				kayttaja = kayttaja.substring(1);
				
			if(kayttaja.startsWith("#"))
				return false;

			if(kayttaja.equals(nick.toLowerCase()))
				return true;
			else return false;
		}

	public static boolean isChannelMsg(String mejo) { // HAKEE ONKO KANAVAVIESTI
		if(mejo.startsWith(":"))
			mejo = mejo.substring(1);
		else return false;
		// :nick!mask@host.com PRIVMSG #kanava :viesti tähän riviin
		String[] osat = mejo.split(" ");
		int taulun_koko = osat.length;

		if(taulun_koko < 4)
			return false;

		String komento = osat[1];

		if(!komento.equals("PRIVMSG")) // ei ole viesti
			return false;

		String kanava = osat[2].toLowerCase();

		if(!kanava.startsWith("#"))
			return false;

		else return true;
		}

	public static boolean isChannelAction(String mejo) {

		String[] osat = mejo.split(" ");
		if(osat.length < 3)
			return false;
		String kanava = osat[2];

		if(kanava.startsWith(":"))
			kanava = kanava.substring(1);

		if(!kanava.startsWith("#"))
			return false;

		if(osat[1].equals("NOTICE") || osat[1].equals("MODE") || osat[1].equals("JOIN") || osat[1].equals("PART") || osat[1].equals("TOPIC") || osat[1].equals("KICK"))
			return true;

		if(osat[1].equals("PRIVMSG") && osat.length > 3) {

			if(osat[3].contains("ACTION"))
				return true;
		}

		return false;


	}
		
	public static boolean isCommand(String mejo) {

		String[] commands = {"!op","!dop","!voice","!devoice","!topic","!kick","!ban","!kickban","!dontlog","!savelog","!mute","!unmute"};

		for(int i = 0; i < commands.length; i++) {
			
			if(mejo.startsWith(commands[i]))
				return true;
		}
		return false;
	}

	public static boolean isAction(String mejo) {
		
		if(isCommand(mejo))
			return true;

		if(mejo.startsWith("!stats") || mejo.startsWith("!random"))
			return true;
		else
			return false;
	}
}
