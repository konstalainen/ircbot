### What is this repository for? ###

* This is a Java-based IRC bot
* Version: 0.7.2 (beta)
* Properties
* * Control bot via IRC private messags
* * Register users with username & password
* * Gives op / voice to desired users on channels
* * Logs the conversations and generates stats in real time
* * Enable/disable logging on channels
* * Gets and send messages from buffer file
* * * = you can send messages on web via PHP-form
* * Generates an HTML-page from detailed stats
* * Error logs for debugging
* * Easy program for setting the bot up with desired information

### Tehtävälista ###

* Tee ohjekirja (manuaali) - ei ole helppo käyttää, jos ei tiedä, mitä tekee
* Luo PHP-sivun pohja, jotta käyttäjät voivat lähettää viestejä botin kautta (esim PHP-curl tms)
* Muuta parsitut IRC-viestit oliotyyppisiksi, jottei staattisten luokkien metodeja tarvi kutsua joka välissä
* Joitain alaluokkia koodin selventämiseksi
* Botin pitää kertoa apua käyttäjälle ja adminille, kun sitä pyydetään *help*-komennolla
* Joitain asetuksia, kuten PHP-tiedoston sijainnin tai logituskansion, pitäisi pysytä vaihtamaan lennosta bottia uudelleenkäynnistämättä.

### Contacts ###

* antti.maenpanen@student.uwasa.fi // maenpanen9@gmail.com
* ypsilon @ QuakeNet for user info
# README #

### To compile (linux): ###

```
#!bash

$ cd /path/to/ircbot_root/
$ ./compile.sh
```

### To run (linux): ###

```
#!bash

$ cd /path/to/ircbot_root/
$ ./run.sh
```
OR

```
#!bash

$ cd /path/to/ircbot_root
$ java Start
```

### To set up (linux): ###

```
#!bash

$ cd /path/to/ircbot_root/
$ ./setup.sh
```
OR

```
#!bash

$ cd /path/to/ircbot_root
$ java Config
```
