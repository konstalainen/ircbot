package ircbot.helpers;

import java.io.*;
import java.net.*;
/**
 * URL-lukijaa ajetaan rinnakkain Botin kanssa, se lukee puolen minuutin välein netistä
 * löytyvän puskurin sisällön ja lähettää viestin puskurin mukaan.
 * 
 * Uusi URL-lukija luodaan aina, kun uusi yhteys (Socket) luodaan, sillä lukija käyttää samaa
 * kirjoittajaluokkaa kuin Botti.
 * 
 * @author Konsta Mäenpänen <maenpanen9@gmail.com>
 * @version 1.1
 */
public final class UrlLukija implements Runnable {

private BufferedWriter writer;
private String url;

public UrlLukija(BufferedWriter kirjoittaja, String url) {
	try {
		this.writer = kirjoittaja;
		this.url = url;
	}
	catch(Exception e) {
		System.out.println("!>> Error while creating a BufferedWriter ");
		Logger.log("Writer error in URL formation", 8);
		Logger.log(e, 8);}
}

public void run() {

while(true) {
		if(this.url.length() == 0)
			continue;
		if(writer == null) {
			System.out.println("!>> No writer!");
			Logger.log("No writer for URL reader", 8);
			break;
		}
		String timestamp = "";
		try {
			
			Thread.sleep(30000);
			
			URL url = new URL(this.url + "?read=1");
			InputStream url_stream = url.openConnection().getInputStream();
			BufferedReader url_reader = new BufferedReader(new InputStreamReader(url_stream));
			
			String url_line = null;
			//System.out.println("Avattiin url lukua varten.");
			while((url_line = url_reader.readLine()) != null) {
				
				String[] rivin_osat = url_line.split("\\|");
				
				if(rivin_osat.length > 3) {
					
					System.out.println("Command: " + rivin_osat[0]);
					System.out.println("Timestamp: " + rivin_osat[1]);
					System.out.println("Receiver: " + rivin_osat[2]);
					System.out.println("Message: " + rivin_osat[3]);
					
					timestamp = rivin_osat[1];
					
					writer.write("PRIVMSG " + rivin_osat[2] + " :" + rivin_osat[3] + "\r\n");
					writer.flush();
					
					}
					
				System.out.println("URL read: "  + url_line);
				
				}
				
			url_reader.close();
			
			}
			catch(MalformedURLException e) {
				
				System.out.println("!>> Malformed URL");
				Logger.log("Malformed URL", 5);
				Logger.log(e, 5);
				
			} catch(IOException e) {
				
				Logger.log("Error in URL reader", 8);
				Logger.log(e, 8);
				System.out.println("!>> Error in URL reader!");
				
			} catch(Exception e) {
				
				Logger.log("Error in URL reader", 8);
				Logger.log(e, 8);
				System.out.println("!>> Error in URL reader!");
			}	
			
		if(timestamp.length() > 0) { // Poistaa viestin, jos viesti luettiin
			
			System.out.println("Erasing message with timestamp " + timestamp + " from the buffer.");
			
			try {
				
				URL url_clear = new URL(this.url + "?clear=" + timestamp);
				InputStream url_stream = url_clear.openConnection().getInputStream();
				BufferedReader url_reader = new BufferedReader(new InputStreamReader(url_stream));
				String url_line = null;
				
				url_reader.close();
				
				} catch(MalformedURLException e) {
					System.out.println("!>> Malformed URL");
					Logger.log("Malformed URL", 5);
					Logger.log(e, 5);
				} catch(Exception e) {
					Logger.log("Error in URL reader", 8);
					Logger.log(e, 8);
					System.out.println("!>> Error in URL reader!");
				}
			}
	}
	
Logger.log("URL reader has stopped", 8);
System.out.println("!>> URL reader has stopped!");
}

} 
