package ircbot.application;

import java.io.*;
import java.util.*;
import java.io.Serializable;

public class Tietokanta implements Serializable {
	
	private ArrayList<Kayttaja> kayttajat;
	
/**
 * 
 * Generates a new database with an empty @link{Kayttaja}-type ArrayList
 *
 */
	public Tietokanta() {
		this.kayttajat = new ArrayList<Kayttaja>();
	}
	
/**
 *
 * Generates a database from a @link{Kayttaja}-ArrayList
 *
 **/
	public Tietokanta(ArrayList<Kayttaja> kayttajat) {
		this.kayttajat = kayttajat;
	}
/**
 *
 * Get the index in array by nick of the user
 *
 * @return User index as integer in the array, -1 if not found
 *
 **/
	public int nickIndex(String nick) {
	
		nick = nick.toLowerCase();
		int indeksi = -1;
		
		for(int i = 0; i < this.kayttajat.size(); i++) {
		
			if(this.kayttajat.get(i).getNick().equals(nick))
				return i;
		}
		return indeksi;
	}
	
/**
 *
 * Get the index in array by hostmask
 *
 * @return User index as integer in the array, -1 if not found
 **/
	public int hostIndex(String host) {
		
		host = host.toLowerCase();
		
		if(host.equals(""))
			return -1;
		
		for(int i = 0; i < this.kayttajat.size(); i++) {
			
			if(this.kayttajat.get(i).getHost().toLowerCase().equals(host))
				return i;
		}
		
		return -1;
	}
	
/**
 *
 * Checks if user with given nick is found
 *
 * @return true if is found
 *
 **/
	public boolean nickFound(String nick) {
	
		nick = nick.toLowerCase();
		
		if(this.nickIndex(nick) == -1)
			return false;
		else 
			return true;
	}
/**
 * Checks if user-password combination is correct and then logins with this hostmask
 *
 * @return true if match found
 *
 **/
	public boolean login(String nick, String password, String hostmask) {
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
			
		else 
			return this.kayttajat.get(index).login(password, hostmask);

	}

/**
 * Checks if nick-password combo is correct and then logs out from a hostmask
 *
 * @return true if password was correct and user was found
 *
 **/
	public boolean logout(String nick, String password) {
	
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
		
		else
			return this.kayttajat.get(index).logout(password);
	}
	
/**
 *
 * Changes the user password, check with current password
 *
 * @return true if nick was found, and passwords were correct
 *
 **/
	public boolean changepass(String nick, String oldpass, String newpass) {
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
			
		else 
			return this.kayttajat.get(index).changepass(oldpass, newpass);
	}
/**
 *
 * Checks if the given host has admin rights
 *
 * @return true if host is admin
 *
 **/
	public boolean isHostAdmin(String host) {
		
		int index = this.hostIndex(host.toLowerCase());
		
		if(index == -1)
			return false;
			
		else 
			return this.kayttajat.get(index).isAdmin();
	}
	
/**
 *
 * Gives the admin rights to given nick
 *
 * @return true if successful
 *
 **/
	public boolean addAdmin(String nick) {
		
		nick = nick.toLowerCase();
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
			
		else {
		
			if(this.kayttajat.get(index).isAdmin())
				return false;
				
			this.kayttajat.get(index).addAdmin();
			return true;
		}
	
	}
	
/**
 *
 * Removes the admin rights of given nick
 *
 * @return true if successful
 *
 **/
	public boolean delAdmin(String nick) {
		
		nick = nick.toLowerCase();
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
			
		else {
		
			if(!this.kayttajat.get(index).isAdmin())
				return false;
			
			this.kayttajat.get(index).delAdmin();
			return true;
		}
	
	}
	
/**
 *
 * Checks if given host has operator rights on the channel
 *
 * @return true if operator
 *
 **/
	public boolean isHostOp(String host, String channel) {
		
		if(this.hostIndex(host.toLowerCase()) == -1)
			return false;
			
		else
			return this.kayttajat.get(this.hostIndex(host.toLowerCase())).isOp(channel.toLowerCase());
	}
	
/**
 *
 * Checks if given host is voiced on the channel
 *
 * @return true if voice
 *
 **/
	public boolean isHostVoice(String host, String channel) {
		
		if(this.hostIndex(host.toLowerCase()) == -1)
			return false;
			
		else
			return this.kayttajat.get(this.hostIndex(host.toLowerCase())).isVoice(channel.toLowerCase());
	}


/**
 *
 * Adds operator rights to given nick to given channel
 *
 * @return true if successful
 *
 **/
	public boolean addOp(String nick, String channel) {
		
		nick = nick.toLowerCase();
		channel = channel.toLowerCase();
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
			
		else
			return this.kayttajat.get(index).addOp(channel);
		
	}

/**
 *
 * Removes operator rights of the user from the given channel
 *
 * @return true if successful
 *
 **/
	public boolean delOp(String nick, String channel) {
	
		nick = nick.toLowerCase();
		channel = channel.toLowerCase();
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;

		else
			return this.kayttajat.get(index).delOp(channel);
		
	}
	
/**
 *
 * Adds voice to given nick to given channel
 *
 * @return true if successful
 *
 **/
	public boolean addVoice(String nick, String channel) {
	
		nick = nick.toLowerCase();
		channel = channel.toLowerCase();
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
			
		else	
			return this.kayttajat.get(index).addVoice(channel);
	}
	
/**
 *
 * Removes voice of the user from the given channel
 *
 * @return true if successful
 *
 **/
	public boolean delVoice(String nick, String channel) {
	
		nick = nick.toLowerCase();
		channel = channel.toLowerCase();
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
		
		else
			return this.kayttajat.get(index).delVoice(channel);
	
	}
	
/**
 *
 * Adds new user, checks for nick duplicates before adding
 *
 * @return true if successful
 *
 **/
	public boolean addUser(Kayttaja kayttaja) {
		
		if((this.nickIndex(kayttaja.getNick()) != -1) || (kayttaja.getNick().length() == 0))
			return false;
			
		else {
			this.kayttajat.add(kayttaja);
			return true;
			}
	}
/**
 *
 * Removes user with given nickname
 *
 * @return true if successful
 *
 **/
	public boolean delUser(String nick) {
		
		nick = nick.toLowerCase();
		
		int index = this.nickIndex(nick);
		
		if(index == -1)
			return false;
			
		else {
			this.kayttajat.remove(index);
			return true;
			}
			
	}

/**
 *
 * Tells number of registered users
 *
 * @return integer value, size of the @link{Kayttaja}-list
 *
 **/
	public int userCount() {
		return this.kayttajat.size();
	}
	
/**
 * @return user in the given index
 **/
	public Kayttaja getUserByIndex(int index) {
	
		if(index >= 0 && index < this.kayttajat.size())
			return this.kayttajat.get(index);
		
		else return null;
	}
}
