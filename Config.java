import ircbot.setup.Setup;
import ircbot.helpers.Logger;

public class Config {
	public static void main(String[] args) {
		try {
			Setup.run();
		} catch(Exception e) {
		Logger.log("Fatal error!", 8);
		Logger.log(e, 8);
		}
	}
}
