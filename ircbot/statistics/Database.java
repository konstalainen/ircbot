package ircbot.statistics;
import java.util.*;
import java.io.*;
import java.text.*;
import ircbot.helpers.Logger;
import ircbot.helpers.Tiedosto;

public class Database {

	private ArrayList<Message> messages;
	private Map<String, Integer> occurrences;
	String channel;

	public Database(String channel, String logpath) {

		this.channel = channel;
		messages = new ArrayList<Message>();
		occurrences = new LinkedHashMap<String, Integer>();

		try {
		
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(logpath + ".log"), "utf-8"));
			String temp_row;
			Message temp_message;
			
			while((temp_row = reader.readLine()) != null) {
			
				temp_message = new Message(temp_row);

				if(!temp_message.malformedMessage())
					messages.add(temp_message);

				}

			reader.close();

			} catch(ParseException e) {
				Logger.log(e, 8);
				//e.printStackTrace();
			}catch(NullPointerException e) {
				Logger.log(e, 8);
				//e.printStackTrace();
			} catch(FileNotFoundException e) {
				Logger.log(e, 8);
				//e.printStackTrace();
			} catch(IOException e) {
				Logger.log(e, 8);
				//e.printStackTrace();
			} catch(Exception e) {
				Logger.log(e, 8);
				//e.printStackTrace();
			}

		this.countOccurrences();
	}


	public Database(ArrayList<Message> messages, String channel) {
		this.channel = channel;
		this.messages = messages;
		this.countOccurrences();
	}

	public void countOccurrences() {

		Map<String, Integer> temp_map = new LinkedHashMap<String, Integer>();
		String str;
		int num;
		for(int i = 0; i < this.messages.size(); i++) {

			str = this.messages.get(i).nick;

			if(temp_map.containsKey(str)) {
				num = (int) temp_map.get(str);
				num++;
				temp_map.put(str, new Integer(num));
			}

			else {
				temp_map.put(str, new Integer(1));
			}

		}

		this.occurrences = temp_map;
		this.orderOccurrences(true);
	}

	public void orderOccurrences(boolean desc) {

		Map<String, Integer> temp_map = new LinkedHashMap<String, Integer>();
		ArrayList<String> keys = new ArrayList<String>();
		String temp_key;

		Set set = this.occurrences.entrySet();
      	Iterator iter = set.iterator();

      	while(iter.hasNext()) {
			Map.Entry row = (Map.Entry) iter.next();
			keys.add((String) (row.getKey()));
     	 }

     	 boolean changes = true;
     	 //System.out.println(keys);
     	 while(changes) {

     	 	changes = false;

     	 	for(int i = 0; i < (keys.size() - 1); i++) {

     	 		if((this.occurrences.get(keys.get(i)) < this.occurrences.get(keys.get(i+1))) == desc) {
     	 			changes = true;
     	 			temp_key = keys.get(i+1);
     	 			keys.set(i+1, keys.get(i));
     	 			keys.set(i, temp_key);
     	 		}
     	 	}

     	 }

     	 for(int i = 0; i < keys.size(); i++) {
     	 	temp_map.put(keys.get(i), this.occurrences.get(keys.get(i)));
     	 }

     	 this.occurrences = temp_map;

	}

	public String randomLine(String user) {

		Database temp_db = this.filterByNick(user, false);
		int random = (int) Math.floor(Math.random() * temp_db.messages.size());

		if(random == temp_db.messages.size())
			random--;

		if(random < 0)
			return "";

		return temp_db.messages.get(random).contents;

	}


	public Database actionsOnly(boolean mode) {

		ArrayList<Message> temp = new ArrayList<Message>();

		for(int i = 0; i < this.messages.size(); i++) {

			if(this.messages.get(i).isAction == mode) 
				temp.add(this.messages.get(i));

		}

		return new Database(temp, this.channel);
	}

	
	public Database countWords() {
	
		ArrayList<Message> temp = new ArrayList<Message>();
		Database temp_db = this.actionsOnly(false);
		
		for(int i = 0; i < temp_db.messages.size(); i++) {
		
			Message temp_msg = temp_db.messages.get(i);
			String[] splitted = temp_msg.contents.split(" ");
			
			for(int j = 0; j < splitted.length; j++) {
				temp.add(new Message(temp_msg.timestamp, temp_msg.nick, splitted[j], false, Actions.PRIVMSG));
			}
		}
		
		return new Database(temp, this.channel);
		
	}
	
	
	public Database filterByType(int type) {

		if(!Actions.inScope(type))
			return this;

		ArrayList<Message> temp = new ArrayList<Message>();

		for(int i = 0; i < this.messages.size(); i++) {

			if(this.messages.get(i).isAction && (this.messages.get(i).actionType == type)) {
				temp.add(this.messages.get(i));
			}
		}

		return new Database(temp, this.channel);

	}
	
	
	public Database filterBySlaps() {
		
		Database temp_db = this.filterByType(Actions.PRIVMSG);
		ArrayList<Message> temp = new ArrayList<Message>();
		
		for(int i = 0; i < temp_db.messages.size(); i++) {
		
			if(temp_db.messages.get(i).contents.toLowerCase().startsWith("slaps")) {
			
				Message temp_message = temp_db.messages.get(i);
				temp_message.contents = "* " + temp_message.nick + " " + temp_message.contents;
				temp.add(temp_message);
				
			}
		}
		
		return new Database(temp, this.channel);
	}


	public Database filterByNick(String nick, boolean casesensitive) {

		ArrayList<Message> temp = new ArrayList<Message>();

		for(int i = 0; i < this.messages.size(); i++) {

			if(casesensitive) {

				if(this.messages.get(i).nick.equals(nick))
					temp.add(this.messages.get(i));
			}

			else {
				
				if(this.messages.get(i).nick.toLowerCase().equals(nick.toLowerCase()))
					temp.add(this.messages.get(i));
			}
			
		}

		return new Database(temp, this.channel);

	}

	public Database limitByDate(Date date, boolean after) {

		ArrayList<Message> temp = new ArrayList<Message>();

		for(int i = 0; i < this.messages.size(); i++) {

			if((this.messages.get(i).timestamp.after(date) && after) || (this.messages.get(i).timestamp.before(date) && !after))
					temp.add(this.messages.get(i));
		}

		return new Database(temp, this.channel);

	}


	public String toString() {
		String str = "";
		Set set = this.occurrences.entrySet();
      	Iterator iter = set.iterator();

      	while(iter.hasNext()) {
			Map.Entry row = (Map.Entry) iter.next();
			str = str + row.getKey() + ((((String) row.getKey()).length() < 8) ? "\t" : "") + "\t" + row.getValue() + "\n";
     	 }

		for(int i = 0; i < this.messages.size(); i++)
			str = str + this.messages.get(i) + ((i == this.messages.size() - 1) ? "" : "\n");
		return str;
	}


	public static void writeHtml(String filename, Database db) throws IOException {

		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "utf-8"));
		writer.write(
					"<!DOCTYPE html>\n" +
					"<head>\n" +
					"\t<meta charset='UTF-8'>\n");
		writer.flush();
		writer.write(
			"\t<title>Kanavan " + db.channel + " tilastot</title>\n" + 
			"\t<link rel='stylesheet' type='text/css' href='style.css'>\n" + 
			"</head>\n"+
			"<body>\n<div class='header'>Bot Stats</div>\n<h2>Kanavan " + db.channel + " tilastot</h2>\n" + 
			"<h4>Made by IRCBOT</h4>\n" + 
			"<h3>Aktiivisimmat irkkaajat:</h3>\n");
		writer.flush();
		writer.write(dbToHtmlTable(db.actionsOnly(false), "Rivejä", "rivi", true, 25));
		writer.flush();
		writer.write("<h3>Käyttäjät, joilla eniten asiaa</h3>");
		writer.flush();
		writer.write(dbToHtmlTable(db.countWords(), "Sanoja", "sana", true, 25));
		writer.flush();
		writer.write("<h3>Eniten släppäyksiä</h3>\n");
		writer.flush();
		writer.write(dbToHtmlTable(db.filterBySlaps(), "Lyöntejä", "uhri", true, 10));
		writer.flush();
		writer.write(
				"<h3>Satunnaisia tilastoja</h3>\n" + 
				"\n<table class='stat'>\n" + 
					"\t<tr class='datarow_even'>\n" + 
						"\t\t<td class='text'>" + 
						((db.filterByType(Actions.KICK).messages.size() == 0) ? "Ketään ei ole potkittu tältä kanavalta! :)" : "Kanavan irkkipoliisi on " + dbToHtmlLine(db.filterByType(Actions.KICK), "potki", "kertaa käyttäjän kanavalta!", true, false)) + 
						"</td>\n" +
					"\t</tr>\n");
		writer.flush();
		writer.write(
					"\t<tr class='datarow_odd'>\n" + 
						"\t\t<td class='text'>" + 
						"Viimeisin topic");
		writer.flush();
		Database topic_db = db.filterByType(Actions.TOPIC);
		
		if(topic_db.messages.size() > 0) {
		
			Message latest_topic = topic_db.messages.get(topic_db.messages.size() - 1);
			String latest_topic_date = Message.dateformat.format(latest_topic.timestamp);
			String latest_topic_topic = latest_topic.contents.replaceFirst("set topic: ","");
			String latest_topic_nick = latest_topic.nick;
			writer.write(" " + latest_topic_date + ": <b>" + latest_topic_nick + "</b> asetti topiciksi <i>" + latest_topic_topic + "</i>");  
		}
		
		else
			writer.write(": Topicia ei ole asetettu");
			
		writer.write("</td>\n" +
					"\t</tr>\n" +
				"\n</table>");
		writer.flush();
		
		Date currentDate = new Date(); // Päiväys timestampia varten.
		SimpleDateFormat dateform = new SimpleDateFormat("d.M.yyyy H:mm:ss z");
		TimeZone tz = Tiedosto.getTimeZone();
		dateform.setTimeZone(tz);
		String latest_edited = dateform.format(currentDate); // muuttaa merkkijonoksi
		
		writer.write("\n\n<p><i>Tilastot päivitetty viimeksi " + latest_edited + "</i></p>\n</body>\n</html>");
		writer.flush();
		writer.close();
		System.out.println("Stats saved to html");
	}
		/*
		 SUB-function for writeHtml
		*/
		public static String dbToHtmlTable(Database db, String keyname1, String keyname2, boolean getRandom, int limit) {

			Set set = db.occurrences.entrySet();
			Iterator iter = set.iterator();
			int sija = 1;
			String str = 
				"\n<table class='stat'>\n" + 
					"\t<tr class='header'>\n" + 
						"\t\t<td class='pos'>#</td>\n" + 
						"\t\t<td class='nick'>Nimi</td>\n" + 
						"\t\t<td class='number'>" + keyname1 + "</td>\n" + 
						((getRandom) ? "\t\t<td class='text'>Satunnainen " + keyname2 + "</td>\n" : "") +
					"\t</tr>\n";
			
			if(!iter.hasNext()) {
				str = str + 
					"\t<tr class='datarow_even'>\n" +
						"\t\t<td class='text' colspan='" + (getRandom ? 4 : 3) + "'>Tilastoja ei saatavilla</td>\n" + 
					"\t</tr>\n";
				}
			
			while(iter.hasNext() && ((limit == 0) || (sija <= limit))) {
				
				Map.Entry row = (Map.Entry) iter.next();
				
				str = str + 
					"\t<tr class='datarow_" + ((sija % 2 == 0) ? "even" : "odd") + "'>\n" +
						"\t\t<td class='pos'>" + sija + "</td>\n" +
						"\t\t<td class='nick'><b>" + row.getKey() + "</b></td>\n" +
						"\t\t<td class='number'>" + row.getValue() + "</td>\n" +
						((getRandom) ? "\t\t<td class='text'><i>" + db.randomLine((String) row.getKey()) + "</i></td>\n" : "") +
					"\t</tr>\n";
					
				sija++;
			}
			
			str = str + "</table>\n\n";
			
			return str;
		}
		
		/*
		 SUB-function for one HTML-styled line
		*/
		public static String dbToHtmlLine(Database db, String prevalue, String postvalue, boolean printNick, boolean getRandom) {
			
			Set set = db.occurrences.entrySet();
			Iterator iter = set.iterator();
			String str = "";
			
			if(iter.hasNext()) {
				
				Map.Entry row = (Map.Entry) iter.next();
				
				str = "<b>" + row.getKey() + ":</b> " + prevalue + " " + row.getValue() + " " + postvalue +
				((getRandom) ? (" <i>" + (printNick ? row.getKey() + " " : "") + db.randomLine((String) row.getKey()) + "</i>") : "");
			}
			
			return str;
		}


	public static void create(String topic, String logpath, String outputfile) {
		try {
			Database testbase = new Database(topic, logpath);
			testbase.writeHtml(outputfile, testbase);
		} catch(IOException e) {
			Logger.log("Error while trying to create HTML statistics", 6);
			Logger.log(e, 6);
			//e.printStackTrace();
		}

	}

}
