package ircbot.helpers;

import java.io.*;
import java.net.*;
import java.util.*;
import java.text.SimpleDateFormat;
import ircbot.application.*;

public class Tiedosto {

	public static void luoHakemisto(String hakemisto) {
	
		File bin_kansio = new File(hakemisto);
		
		try{
			
			if(bin_kansio.mkdir()) { 
				
				System.out.println("Folder " + hakemisto + " created");
				Logger.log("Folder " + hakemisto + " created", 4);
				
			} else {
				
				System.out.println("!>> Folder " + hakemisto + " not created. It may already exist");
				Logger.log("Failed to create folder " + hakemisto + ": it already exists", 6);
				
			}
		} catch(Exception e){
			
			System.out.println("!>> Failed to create folder " + hakemisto);
			Logger.log("Failed to create folder", 6);
			Logger.log(e, 6);
			// e.printStackTrace();
		} 
	}

	public static Tietokanta lataaKayttajat() {
	
	Tietokanta kayttajat = new Tietokanta();
	
	try { // Yritetään ladata
		
		FileInputStream fileIn = new FileInputStream("bin/users.dat");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		kayttajat = (Tietokanta) objectIn.readObject();
		objectIn.close();
		fileIn.close();
		System.out.println("Users loaded");
		Logger.log("Users loaded", 4);
		
	} catch (FileNotFoundException e) {
		
		System.out.println("!>> User file (bin/users.dat) not found!");
		Logger.log("Userfile (bin/users.dat) not found!", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
		
	} catch (Exception e) {
		
		System.out.println("!>> Failed to load users");
		Logger.log("Failed to load users", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
		
	}
	
	return kayttajat;
}

public static boolean tallennaKayttajat(Tietokanta kayttajat) {

	FileOutputStream fileOut = null;
	ObjectOutputStream objectOut = null;
	
    try {
		
       fileOut = new FileOutputStream("bin/users.dat");
       objectOut = new ObjectOutputStream(fileOut);
       objectOut.writeObject(kayttajat);
       objectOut.close();
       fileOut.close();
       System.out.println("Users saved");
	   Logger.log("Users saved", 2);
       return true;
       
    } catch (FileNotFoundException e) {
		
    	System.out.println("!>> User file (bin/users.dat) not found!");
		Logger.log("Userfile (bin/users.dat) not found!", 6);
		Logger.log(e, 6);
    	// e.printStackTrace();
    	
    } catch(Exception e) {
		
    	System.out.println("!>> Failed to save users");
		Logger.log("Failed to save users", 6);
		Logger.log(e, 6);
    	// e.printStackTrace();
    	
       }
	   
    return false;
	
}

public static ArrayList<String> lataaPalvelimet() {
	
	ArrayList<String> serverit = new ArrayList<String>();
	
	try {
		
		FileInputStream fileIn = new FileInputStream("bin/servers.dat");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		serverit = (ArrayList<String>) objectIn.readObject();
		
		objectIn.close();
		fileIn.close();
		
		System.out.println("Servers loaded");
		Logger.log("Servers loaded", 4);
		
	} catch(FileNotFoundException e) {
		
		System.out.println("!>> Server file (bin/servers.dat) not found!");
		Logger.log("Server file (bin/servers.dat) not found!", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
		
	} catch(Exception e) {
		
		System.out.println("!>> Failed to load servers!");
		Logger.log("Failed to load servers", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
	}
	
	return serverit;
}

public static boolean tallennaPalvelimet(ArrayList<String> serverit) {

	FileOutputStream fileOut = null;
	ObjectOutputStream objectOut = null;
	
    try {
		
       fileOut = new FileOutputStream("bin/servers.dat");
       objectOut = new ObjectOutputStream(fileOut);
       objectOut.writeObject(serverit);
       
       objectOut.close();
       fileOut.close();
       
       System.out.println("Servers saved");
	   Logger.log("Servers saved", 4);
	   
       return true;
       
    } catch(FileNotFoundException e) {
		
    	System.out.println("!>> Server file (bin/servers.dat) not found!");
		Logger.log("Server file (bin/servers.dat) not found!", 6);
		Logger.log(e, 6);
    	// e.printStackTrace();
    	
    } catch(Exception e) {
		
    	System.out.println("!>> Failed to save servers!");
		Logger.log("Failed to save servers", 6);
		Logger.log(e, 6);
    	// e.printStackTrace();
    	
       }
    return false;
}

public static ArrayList<Kanava> lataaKanavat() {

	ArrayList<Kanava> channels = new ArrayList<Kanava>();
	
	try {
		
		FileInputStream fileIn = new FileInputStream("bin/channels.dat");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		channels = (ArrayList<Kanava>) objectIn.readObject();
		
		objectIn.close();
		fileIn.close();
		
		System.out.println("Channels loaded");
		Logger.log("Channels loaded", 4);
		
	} catch(FileNotFoundException e) {
		
		System.out.println("!>> Channel file (bin/channels.dat) not found!");
		Logger.log("Channel file (bin/channels.dat) not found!", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
		
	} catch(Exception e) {
		
		System.out.println("!>> Failed to load channel list!");
		Logger.log("Failed to load channel list", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
	}
	
	return channels;
}


public static boolean tallennaKanavat(ArrayList<Kanava> channels) {

	FileOutputStream fileOut = null;
	ObjectOutputStream objectOut = null;
	
    try {
		
		fileOut = new FileOutputStream("bin/channels.dat");
		objectOut = new ObjectOutputStream(fileOut);
		objectOut.writeObject(channels);
		
		objectOut.close();
		fileOut.close();
		
		System.out.println("Channels saved");
		Logger.log("Channels saved", 3);
		
		return true;
		
    } catch(FileNotFoundException e) {
		
    	System.out.println("!>> Channel file (bin/channels.dat) not found!");
		Logger.log("Channel file (bin/channels.dat) not found!", 6);
		Logger.log(e, 6);
    	// e.printStackTrace();
    	
    } catch(Exception e) {
		
    	System.out.println("!>> Failed to save channel list!");
		Logger.log("Failed to save channel list", 6);
		Logger.log(e, 6);
    	// e.printStackTrace();
    	
       }
    return false;
}
/*
public static ArrayList<String> lataaNimimerkit() {

	ArrayList<String> nickLista = new ArrayList<String>();
	try {
		FileInputStream fileIn = new FileInputStream("bin/botnicks.dat");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		nickLista = (ArrayList<String>) objectIn.readObject();
		objectIn.close();
		fileIn.close();
		System.out.println("Nicknames loaded");
	} catch(FileNotFoundException e) {
		System.out.println("!>> Nickname file (bin/botnicks.dat) not found!");
		e.printStackTrace();
	} catch(Exception e) {
		System.out.println("Failed to load nicknames!");
		e.printStackTrace();
	}
	
	return nickLista;
	
}

public static void tallennaNimimerkit(ArrayList<String> nickLista) {

	try {
		FileOutputStream fileOut = new FileOutputStream("bin/botnicks.dat");
		ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
		objectOut.writeObject(nickLista);
		objectOut.close();
		fileOut.close();
		System.out.println("Oletusnimimerkit tallennettu!");
	} catch(FileNotFoundException e) {
		System.out.println("Botin nimimerkkitiedostoa (bin/botnicks.dat) ei loydy");
		e.printStackTrace();
	} catch(Exception e) {
		System.out.println("Virhe tallennettaessa oletusnimimerkkeja");
		e.printStackTrace();
	}
}

public static ArrayList<String> haeTiedot() {

	ArrayList<String> botinfo = new ArrayList<String>();
	
	try {
		FileInputStream fileIn = new FileInputStream("bin/botinfo.dat");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		botinfo = (ArrayList<String>) objectIn.readObject();
		objectIn.close();
		fileIn.close();
	} catch(FileNotFoundException e) {
		System.out.println("Botin infotiedostoa (bin/botinfo.dat) ei loydy");
		e.printStackTrace();
	} catch(Exception e) {
		System.out.println("Virhe ladattaessa botin tietoja");
		e.printStackTrace();
	}
	
	return botinfo;
}

public static void tallennaTiedot(ArrayList<String> botinfo) {

		try {
			FileOutputStream fileOut = new FileOutputStream("bin/botinfo.dat");
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(botinfo);
			objectOut.close();
			fileOut.close();
		} catch(FileNotFoundException e) {
			System.out.println("Botin asetustiedosta ei loydy");
			e.printStackTrace();
		} catch(Exception e) {
			System.out.println("Oletustietojen tallennus epaonnistui");
			e.printStackTrace();
		}
}
*/
public static Bot lataaBotti() {

	Bot botti = new Bot();
	
	try {
		FileInputStream fileIn = new FileInputStream("bin/botinfo.dat");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		botti = (Bot) objectIn.readObject();
		objectIn.close();
		fileIn.close();
		Logger.log("Bot settings loaded", 4);
	} catch(FileNotFoundException e) {
		System.out.println("!>> Bot infofile (bin/botinfo.dat) not found!");
		Logger.log("Bot infofile (bin/botinfo.dat) not found", 6);
		Logger.log(e, 6);
	} catch(Exception e) {
		System.out.println("!>> Failed to load bot settings!");
		Logger.log("Failed to load bot settings", 6);
		Logger.log(e, 6);
	}
	
	return botti;
}

public static TimeZone getTimeZone() {
	
	Bot botti = new Bot();
	
	try {
		
		FileInputStream fileIn = new FileInputStream("bin/botinfo.dat");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		
		botti = (Bot) objectIn.readObject();
		
		objectIn.close();
		fileIn.close();
		
	} catch(FileNotFoundException e) {
		return TimeZone.getTimeZone("GMT");
	} catch(Exception e) {
		return TimeZone.getTimeZone("GMT");
	}
	
	return botti.getTimeZone();
}


public static void tallennaBotti(Bot botti) {

	try {
		
		FileOutputStream fileOut = new FileOutputStream("bin/botinfo.dat");
		ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
		objectOut.writeObject(botti);
		
		objectOut.close();
		fileOut.close();
		
		Logger.log("Bot settings saved", 4);
		
	} catch(FileNotFoundException e) {
		
		System.out.println("!>> Bot infofile (bin/botinfo.dat) not found!");
		Logger.log("Bot infofile (bin/botinfo.dat) not found", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
		
	} catch(Exception e) {
		
		System.out.println("!>> Failed to save bot settings!");
		Logger.log("Failed to save bot settings", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
		
	}

}

public static void tallennaLogi(String basepath, String channel, String nick, String viesti, boolean action, TimeZone timezone) {

	Date currentDate = new Date(); // Päiväys timestampia varten.
	SimpleDateFormat dateform = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // muoto [2016-01-31 23:15:59]
	dateform.setTimeZone(timezone);
	String timestamp = dateform.format(currentDate); // muuttaa merkkijonoksi
	
	channel = channel.toLowerCase();
	String logitus;
	
	if(!basepath.endsWith("/"))
		basepath = basepath + "/";
	
	if(channel.startsWith("#"))
		channel = channel.substring(1);
		
	else
		return;
		
	if(channel.length() < 1)
		return;
		
	if(!action)
		logitus = "[" + timestamp + "]" + "\t<" + nick + ">\t" + viesti;

	else 
		logitus =  "[" + timestamp + "]" + "\t*\t<" + nick + ">\t" + viesti;
	
	try {
		
		PrintWriter logittaja = new PrintWriter(new FileOutputStream(basepath + channel + ".log", true));
		
		logittaja.println(logitus);
		logittaja.close();
		
	} catch(FileNotFoundException e) {
		
		System.out.println("!>> Folder is not ok, log wasn't saved!");
		Logger.log("Can't find log files", 6);
		Logger.log(e, 6);
		
	} catch(Exception e) {
		
		System.out.println("!>> Error while logging conversation!");
		Logger.log("Failed to log conversation", 6);
		Logger.log(e, 6);
		// e.printStackTrace();
		
	}
}

	public static void tallennaLogi(String basepath, String channel, String nick, String viesti, TimeZone tz) {
		tallennaLogi(basepath, channel, nick, viesti, false, tz);
	}
}
