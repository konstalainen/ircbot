@ECHO OFF
ECHO Recompiling the Java source files...
javac -d . -encoding utf-8 ircbot/application/*.java
ECHO Application classes compiled
javac -d . -encoding utf-8 ircbot/setup/*.java
ECHO Setup classes compiled
javac -d . -encoding utf-8 ircbot/helpers/*.java
ECHO Helpers class compiled
javac -d . -encoding utf-8 ircbot/statistics/*.java
ECHO Stat generator classes compiled
javac -d . -encoding utf-8 Start.java
javac -d . -encoding utf-8 Config.java
ECHO Starters compiled
ECHO Ready.
