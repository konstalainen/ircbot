package ircbot.application;

import java.io.*;

public class Kanava implements Serializable {

	private String nimi;
	private boolean logi;
	private String salasana;
	
	public Kanava(String kanava) {
		
		this.nimi = kanava;
		this.salasana = "";
		this.logi = true;
	}
	
	public Kanava(String kanava, boolean logita) {
		this(kanava);
		this.logi = logita;
	}
	
	public Kanava(String kanava, String salasana) {
		this(kanava);
		this.salasana = salasana;
	}
	
	public Kanava(String kanava, String salasana, boolean logita) {
		this(kanava, salasana);
		this.logi = logita;
	}
	
	public void asetaLogi(boolean logita) {
		this.logi = logita;
	}
	
	public String nimi() {
		return this.nimi;
	}
	
	public boolean logita() {
		return this.logi;
	}
	
	public String salasana() {
		return this.salasana;
	}
	
	public void asetaSalasana(String salasana) {
		this.salasana = salasana;
	}
	
}
