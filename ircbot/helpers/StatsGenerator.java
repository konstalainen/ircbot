package ircbot.helpers;

import java.io.*;
import java.util.*;
import ircbot.statistics.Database;
import ircbot.application.Kanava;

public final class StatsGenerator implements Runnable {

private ArrayList<Kanava> kanavat;
private ArrayList<Kanava> chan_buffer;
private String logpath;
private String output;
private Object lock = new Object();

public StatsGenerator(ArrayList<Kanava> kanavat, String logpath, String output) {
	this.kanavat = kanavat;
	this.chan_buffer = kanavat;
	this.logpath = logpath;
	this.output = output;
}

public StatsGenerator(ArrayList<Kanava> kanavat) {
	this(kanavat, "logs/", "html/");
}

public void run() {
	//System.out.println("DEBUG: thread started");
	
	try {
		while(true) {
		
			for(int i = 0; i < this.kanavat.size(); i++) {
				
				if(this.kanavat.get(i).logita()) {
				
					//System.out.println("DEBUG: logged channel found");
					
					String channel_name = this.kanavat.get(i).nimi();
					String logfile = (channel_name.startsWith("#") ? channel_name.substring(1) : channel_name);
					
					//System.out.println("Logfile is at: " + this.logpath + logfile + ".log");
					
				
					
					Database.create(channel_name, this.logpath + logfile, this.output + logfile + ".html"); // HTML-tilaston luonti
					
					//System.out.println("Html file saved to " + this.output + logfile + ".html");
					
					System.out.println("Stats generated from " + this.kanavat.get(i).nimi());
					Thread.sleep(1000);
				}
				
			}
			
			//System.out.println("DEBUG: all the channels read. Check if channels added/removed");
			
			synchronized(lock) {
				this.kanavat = this.chan_buffer;
				//System.out.println("DEBUG: channel list updated");
			}
			
			Thread.sleep(1000*60*60);
			//System.out.println("DEBUG: Sleeping before generating again");
		}
	} catch(InterruptedException e) {
		Logger.log("HTML stat generator thread was interrupted", 8);
		Logger.log(e, 8);
	}
	
	System.out.println("!>> HTML stat generator has stopped!");
	Logger.log("HTML stat generator stopped", 8);
}

public void editChannels(Kanava kanava, boolean add) {
	
	synchronized(lock) {
	
		if(add) {
			this.chan_buffer.add(kanava);
			//System.out.println("DEBUG: added channel");
			}
		else {
			this.chan_buffer.remove(kanava);
			//if(this.chan_buffer.remove(kanava))
				//System.out.println("DEBUG: removed channel");
			//else
				//System.out.println("DEBUG: failed to remove channel (not found)");
			}
		
	}
}

} 
