package ircbot.helpers;
import java.util.*;
import java.text.*;
import java.io.*;

public class Logger {

	private static String filename = "botlog.log";

	public static void log(String message) {
		Logger.log(message, 0);
	}
	
	public static void log(String message, int level) {
	
		SimpleDateFormat dateform = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // muoto [2014-11-30 23:15:59]
		TimeZone timezone = Tiedosto.getTimeZone();
		dateform.setTimeZone(timezone);
		String timestamp = dateform.format(new Date()); // muuttaa merkkijonoksi
		
		String[] levels = {
							"recvmsg", // 0=vastaanotettu viesti
							"recvact", // 1=vastaanotettu tapahtuma
							"useract", // 2=käyttäjien muokkaus
							"chanact", // 3=kanavien muokkaus
							"initial", // 4=käynnistystapahtuma
							"sockerr", // 5=yhdistysvirhe
							"fileerr", // 6=lataus/tallennsuvirhe
							"chaterr", // 7=irc-virhe
							"failure" // 8= fatal error
							};
							
		if(level < 0 || level >= (levels.length))
			level = (levels.length) - 1;
			
		try {
			String logitus = timestamp + "\t" + levels[level] + "\t" + message + "\n";
			BufferedWriter logittaja = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename, true), "utf-8"));
			logittaja.write(logitus);
			logittaja.close();
		} catch(FileNotFoundException e) {
			System.out.println("!>> Invalid log file!");
		} catch(Exception e) {
			System.out.println("!>> Failed to log!");
			// e.printStackTrace();
		}
			
	}
	
	public static void log(Exception e, int level) {
		Logger.log("\n\n" + Logger.eToStr(e), level);
	
	}
	
	public static String eToStr(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString(); // stack trace as a string
	}
	
}
